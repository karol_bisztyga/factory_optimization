package com.bisztyga.factory_simulator.reporting.agents;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.exceptions.ReportException;

public class AgentReportTool implements ReportTool {
	
	private final List<Entry> entries = new ArrayList<>();
	private final FactoryAgent ownerFactoryAgent;
	
	
	public AgentReportTool(FactoryAgent ownerFactoryAgent) {
		this.ownerFactoryAgent = ownerFactoryAgent;
	}

	private Entry getCurrentEntry(EmployeeAgent employee) {
		for(int i=0 ; i<entries.size() ; ++i) {
			Entry entry = entries.get(i);
			if(entry.isOpen() && entry.getEmployee() == employee) {
				return entry;
			}
		}
		return null;
	}
	
	public void startWork(SectorTool sector, EmployeeAgent employee) throws ReportException {
		Entry entry = getCurrentEntry(employee);
		if(entry != null) {
			throw new ReportException("cannot start working for employee " + employee.getEmployee().getId() +
					", already working on sector " + entry.getSector().getSector().getId());
		}
		MainController.log("employee "+employee.getEmployee().getId()+" starts working on "+sector.getSector().getId());
		entries.add(new Entry(employee, sector));
	}
	
	public void stopWork(EmployeeAgent employee) throws ReportException {
		Entry entry = getCurrentEntry(employee);
		if(entry == null) {
			throw new ReportException("cannot stop working for employee " + employee.getEmployee().getId() +
					", they are not working");
		}
		MainController.log("employee "+employee.getEmployee().getId()+" stopped working");
		entry.end();
	}
	
	public int getWorkedTime(EmployeeAgent employee) {
		int sum = 0;
		for(Entry entry : entries) {
			if(entry.getEmployee() == employee) {
				sum += entry.getPeriod();
			}
		}
		return sum;
	}
	
	public int getWorkedTime(SectorTool sector) {
		int sum = 0;
		for(Entry entry : entries) {
			if(entry.getSector() == sector) {
				sum += entry.getPeriod();
			}
		}
		return sum;
	}
	
	@Override
	public void report() {
		MainController.log(this + " start");
		for(int i=0 ; i<entries.size() ; ++i) {
			Entry entry = entries.get(i);
			MainController.log("	" + entry.toString());
		}
		MainController.log(this + " EMPLOYEES");
		for(EmployeeAgent employee : ownerFactoryAgent.getEmployeeAgents()) {
			MainController.log("employee "+employee.getEmployee().getId()+": "+getWorkedTime(employee));
		}
		MainController.log(this + " SECTORS");
		for(SectorTool sector : ownerFactoryAgent.getSectorTools()) {
			MainController.log("employee "+sector.getSector().getId()+": "+getWorkedTime(sector));
		}
		MainController.log(this + " SAVING DATA TO FILE");
		for(Entry entry : entries) {
			String date = getMillisecondsOfCurrentDay()+"";
			String sql = date + ","
					+ entry.getSector().getOwnerFactoryAgent().getFactoryModel().getId() + ","
					+ entry.getEmployee().getEmployee().getId() + ","
					+ entry.getSector().getSector().getId() + ","
					+ entry.getPeriod();
			MainController.getInstance().appendQueryForAnalysis(date, sql, ownerFactoryAgent);
		}
		MainController.log(this + " end");
	}
	
	@Override
	public String toString() {
		return "AgentReportTool [factory="+ ownerFactoryAgent.getFactoryModel().getAddress() +"]";
	}
	
	private static long getMillisecondsOfCurrentDay(int dayDelay) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.DATE, c.get(Calendar.DATE)+dayDelay);
		return c.getTime().getTime();
	}

	private static long getMillisecondsOfCurrentDay() {
		return getMillisecondsOfCurrentDay(7);
	}
	
	public class Entry {
		private final EmployeeAgent employee;
		private final SectorTool sector;
		private final int startTime;
		private Integer endTime = null;
		public Entry(EmployeeAgent employee, SectorTool sector) {
			this.employee = employee;
			this.sector = sector;
			startTime = sector.getOwnerFactoryAgent().getClock().getCurrentMinute();
		}
		
		public EmployeeAgent getEmployee() {
			return employee;
		}

		public SectorTool getSector() {
			return sector;
		}

		public boolean isOpen() {
			return (endTime == null);
		}
		
		public Integer getEndTime() {
			return endTime;
		}

		public void setEndTime(Integer endTime) {
			this.endTime = endTime;
		}

		public int getStartTime() {
			return startTime;
		}

		public void end() {
			endTime = sector.getOwnerFactoryAgent().getClock().getCurrentMinute();
		}
		
		public int getPeriod() {
			if(isOpen()) return 0;
			return getEndTime() - getStartTime();
		}

		@Override
		public String toString() {
			return "Entry [employee=" + employee.getEmployee().getId() + ", sector=" + 
					sector.getSector().getId() + ", period=" + getPeriod() + "]";
		}
		
	}
	
}
