package com.bisztyga.factory_simulator.reporting.agents;

public interface ReportTool {
	public void report();
}
