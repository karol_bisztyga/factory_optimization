package com.bisztyga.factory_simulator.tools;

import java.util.concurrent.TimeUnit;

import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.app.MainController;

public class Clock implements Runnable {
	
	public static final int MINUTE_PERIOD_MS = 10;
	private final int workingMinutes;
	private int currentMinute = 0;
	private final FactoryAgent factoryAgent;
	private boolean working = true;
	
	public Clock(int workingMinutes, FactoryAgent factoryAgent) {
		this.workingMinutes = workingMinutes;
		this.factoryAgent = factoryAgent;
	}

	@Override
	public void run() {
		try {
			while(currentMinute < workingMinutes) {
				setCurrentMinute(getCurrentMinute()+1);
				TimeUnit.MILLISECONDS.sleep(MINUTE_PERIOD_MS);
				factoryAgent.getSocketServer().sendClockToAll(factoryAgent, this);
			}
			this.working = false;
			factoryAgent.endDayOfWork();
		} catch (InterruptedException e) {
			MainController.log("Clock stopped");
		}
	}

	public synchronized int getCurrentMinute() {
		return currentMinute;
	}

	public synchronized void setCurrentMinute(int currentMinute) {
		this.currentMinute = currentMinute;
	}

	public synchronized int getWorkingMinutes() {
		return workingMinutes;
	}

	public boolean isWorking() {
		return working;
	}
	
}
