package com.bisztyga.factory_simulator.communication.sockets;

import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.communication.sockets.tools.IntroducingData;
import com.bisztyga.factory_simulator.communication.sockets.tools.Message;
import com.bisztyga.factory_simulator.communication.sockets.tools.Message.TYPE;
import com.bisztyga.factory_simulator.communication.sockets.tools.wrappers.NewState;
import com.bisztyga.factory_simulator.tools.Clock;
import com.google.gson.Gson;

public class WebsocketServer extends WebSocketServer {

    private static int TCP_PORT = 1234;
    private Map<WebSocket, FactoryAgent> clients = new HashMap<>();
    private final List<FactoryAgent> factoryAgents = new ArrayList<>();
    private final Gson gson = new Gson();

    public WebsocketServer() {
        super(new InetSocketAddress(TCP_PORT));
    }

    @Override
    public void onOpen(WebSocket connection, ClientHandshake handshake) {
    	clients.put(connection, null);
    }

    @Override
    public void onClose(WebSocket connection, int code, String reason, boolean remote) {
    	clients.remove(connection);
    }

    @Override
    public void onMessage(WebSocket connection, String message) {
    	//assign socket to factory on requested factory id or if id  is invalid, err message back to client
    	String[] messageArray = message.split(":");
    	String key = messageArray[0];
    	String value = messageArray[1];
    	if(key.equals("factory")) {
    		int factoryId = Integer.parseInt(value);
        	if(!assignFactoryAgentToClient(connection, factoryId)) {
        		sendMessage(connection, new Message(TYPE.ERROR, "such factory does not exist"));
        	}
    	}
    }
    
    private boolean assignFactoryAgentToClient(WebSocket connection, int factoryId) {
		for(int i=0 ; i<factoryAgents.size() ; ++i) {
			FactoryAgent factoryAgent = factoryAgents.get(i);
			if(factoryAgent.getFactoryModel().getId() == factoryId) {
				clients.replace(connection, factoryAgent);
				sendIntroducingData(connection, factoryAgent);
				return true;
			}
		}
		return false;
    }

    @Override
    public void onError(WebSocket connection, Exception ex) {
        if (connection != null) {
        	clients.remove(connection);
        }
    }
    
    private void sendIntroducingData(WebSocket socket, FactoryAgent factoryAgent) {
    	IntroducingData introducingData;
		try {
			Map<Integer, STATE> employeesStates = new HashMap<>();
			for(int i=0 ; i<factoryAgent.getEmployeeAgents().size() ; ++i) {
				EmployeeAgent agent = factoryAgent.getEmployeeAgents().get(i);
				employeesStates.put(agent.getEmployee().getId(), agent.getState());
			}
			introducingData = new IntroducingData(
					factoryAgent.getFactoryModel(), 
					factoryAgent.getWorkingData(), 
					employeesStates);
	    	sendMessage(socket, new Message(TYPE.INTRODUCING_DATA, gson.toJson(introducingData)));
	    	if(!factoryAgent.getClock().isWorking()) {
	    		sendMessage(socket, new Message(TYPE.CLOCK, "0"));
	    	}
		} catch (SQLException e) {
			MainController.log("failed to pull data from database for factory of id: "
					+factoryAgent.getFactoryModel().getId());
		}
    	
    }
    
    public void spreadEmployeeState(FactoryAgent factoryAgent, NewState newState) {
    	List<WebSocket> connections = getConnectionsByFactoryAgent(factoryAgent); 
    	for(int i=0 ; i<connections.size() ; ++i) {
    		WebSocket client = connections.get(i);
    		sendMessage(client, new Message(
    				TYPE.EMPLOYEE_NEW_STATE, 
    				gson.toJson(newState)));
    	}
    }
    
    public void sendClockToAll(FactoryAgent factoryAgent, Clock clock) {
    	List<WebSocket> connections = getConnectionsByFactoryAgent(factoryAgent); 
    	for(int i=0 ; i<connections.size() ; ++i) {
    		WebSocket client = connections.get(i);
    		sendMessage(client, new Message(TYPE.CLOCK, clock.getCurrentMinute()+"/"+clock.getWorkingMinutes()));
    	}
    }
    
    public void sendEndOfTheDayToAll(FactoryAgent factoryAgent) {
    	List<WebSocket> connections = getConnectionsByFactoryAgent(factoryAgent); 
    	for(int i=0 ; i<connections.size() ; ++i) {
    		WebSocket client = connections.get(i);
    		sendMessage(client, new Message(TYPE.CLOCK, "0"));
    	}
    }
    
    private void sendMessage(WebSocket socket, Message message) {
    	socket.send(gson.toJson(message));
    }
    
    public synchronized void addFactoryAgent(FactoryAgent factoryAgent) {
    	this.factoryAgents.add(factoryAgent);
    }
    
    public List<WebSocket> getConnectionsByFactoryAgent(FactoryAgent factoryAgent) {
		List<WebSocket> result = new ArrayList<>();
		Iterator<Entry<WebSocket, FactoryAgent>> it = clients.entrySet().iterator();
		while (it.hasNext()) {
			Entry<WebSocket, FactoryAgent> pair = it.next();
			if((FactoryAgent)pair.getValue() == factoryAgent) {
				result.add((WebSocket) pair.getKey());
			}
		}
	    return result;
    }

}