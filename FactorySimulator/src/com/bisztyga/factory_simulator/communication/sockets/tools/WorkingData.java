package com.bisztyga.factory_simulator.communication.sockets.tools;

import java.util.HashMap;
import java.util.Map;

public class WorkingData {

	private final Map<Integer, Integer> employeesToSectors = new HashMap<>();

	public void addEmployee(int employeeId) {
		employeesToSectors.put(employeeId, 0);
	}
	
	public boolean employeeNewWorkplace(int employeeId, int sectorId) {
		if(!employeesToSectors.containsKey(employeeId)) {
			return false;
		}
		employeesToSectors.put(employeeId, sectorId);
		return true;
	}
	
	public Integer getEmployeeSector(int employeeId) {
		return employeesToSectors.get(employeeId);
	}
	
	public int getEmployeesCountInSector(int sectorId) {
		int result = 0;
		for(Map.Entry<Integer, Integer> entry : employeesToSectors.entrySet()) {
			if(entry.getValue() == sectorId) {
				++result;
			}
		}
		return result;
	}
	
	public boolean isEmployeeInSector(int employeeId, int sectorId) {
		for(Map.Entry<Integer, Integer> entry : employeesToSectors.entrySet()) {
			if(entry.getKey() == employeeId && entry.getValue() == sectorId) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isSectorOccupied(int sectorId) {
		for(Map.Entry<Integer, Integer> entry : employeesToSectors.entrySet()) {
			if(entry.getValue() == sectorId) return true;
		}
		return false;
	}
	
}
