package com.bisztyga.factory_simulator.communication.sockets.tools;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.database.model.Employee;
import com.bisztyga.factory_simulator.database.model.Factory;
import com.bisztyga.factory_simulator.database.model.FactoryMap;
import com.bisztyga.factory_simulator.database.model.FactoryMapSector;
import com.bisztyga.factory_simulator.database.model.Machine;

public class IntroducingData {

	private final Factory factory;
	private final FactoryMap factoryMap;
	private final List<Employee> employees;
	private final List<FactoryMapSector> sectors;
	private final List<Machine> machines;
	private final WorkingData workingData;
	private final Map<Integer, STATE> employeesStates;
	
	public IntroducingData(Factory factory, WorkingData workingData, Map<Integer, STATE> employeesStates) 
			throws SQLException {
		this.factory = factory;
		factoryMap = MainController.getInstance().getDatabaseManager().getFactoryMap(factory);
		employees = MainController.getInstance().getDatabaseManager().getEmployees(factory.getId());
		sectors = MainController.getInstance().getDatabaseManager().getSectors(factoryMap);
		machines = MainController.getInstance().getDatabaseManager().getMachines(factory);
		this.workingData = workingData;
		this.employeesStates = employeesStates;
	}

	public synchronized Factory getFactory() {
		return factory;
	}

	public synchronized List<Machine> getMachines() {
		return machines;
	}

	public synchronized List<Employee> getEmployees() {
		return employees;
	}

	public FactoryMap getFactoryMap() {
		return factoryMap;
	}

	public List<FactoryMapSector> getSectors() {
		return sectors;
	}

	public WorkingData getWorkingData() {
		return workingData;
	}

	public Map<Integer, STATE> getEmployeesStates() {
		return employeesStates;
	}
	
}
