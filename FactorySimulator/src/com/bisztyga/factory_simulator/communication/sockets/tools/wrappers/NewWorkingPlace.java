package com.bisztyga.factory_simulator.communication.sockets.tools.wrappers;

public class NewWorkingPlace {
	private final int employeeId;
	private final int sectorId;
	public NewWorkingPlace(int employeeId, int sectorId) {
		this.employeeId = employeeId;
		this.sectorId = sectorId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public int getSectorId() {
		return sectorId;
	}
}
