package com.bisztyga.factory_simulator.communication.sockets.tools;

public class Message {
	
	public enum TYPE {
		INTRODUCING_DATA,
		ERROR,
		CLOCK,
		EMPLOYEE_NEW_STATE
	}
	
	private final TYPE type;
	private final Object value;
	
	public Message(TYPE type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	public synchronized TYPE getType() {
		return type;
	}
	
	public synchronized Object getValue() {
		return value;
	}
	
}
