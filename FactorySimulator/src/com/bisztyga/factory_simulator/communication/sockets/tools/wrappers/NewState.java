package com.bisztyga.factory_simulator.communication.sockets.tools.wrappers;

import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;

public class NewState {
	private final int employeeId;
	private final STATE state;
	private final int sectorId;
	public NewState(int employeeId, STATE state, int sectorId) {
		super();
		this.employeeId = employeeId;
		this.state = state;
		this.sectorId = sectorId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public STATE getState() {
		return state;
	}
	public int getSectorId() {
		return sectorId;
	}
}
