package com.bisztyga.factory_simulator.database;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bisztyga.factory_simulator.database.model.Employee;
import com.bisztyga.factory_simulator.database.model.EmployeeEntitlement;
import com.bisztyga.factory_simulator.database.model.Factory;
import com.bisztyga.factory_simulator.database.model.FactoryMap;
import com.bisztyga.factory_simulator.database.model.FactoryMapSector;
import com.bisztyga.factory_simulator.database.model.Machine;
import com.bisztyga.factory_simulator.database.model.MachineEntitlementRequired;
import com.bisztyga.factory_simulator.database.model.WorkingPlace;

public class DatabaseManager {
	
	private String path;
	private Connection connection;
	
	public DatabaseManager() {
		try {
			String folder = System.getProperty("user.dir") + File.separator + ".." + File.separator + "mutualFiles";
			Files.copy(
					new File(folder + File.separator + "db.sqlite3").toPath(), 
					new File(folder + File.separator + "db.sqlite3___COPY").toPath(),
					StandardCopyOption.REPLACE_EXISTING);
			path = folder + File.separator + "db.sqlite3___COPY";
			connection = DriverManager.getConnection("jdbc:sqlite:" + path);
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized ResultSet getRecords(String tableName) {
		return getRecords(tableName, null);
	}
	
	public synchronized ResultSet getRecords(String tableName, String where) {
		ResultSet result;
		try {
			Statement statement = connection.createStatement();
			if(where != null) {
				where = " " + where;
			} else {
				where = "";
			}
			result = statement.executeQuery("SELECT * FROM " + tableName + where);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return result;
	}
	
	public List<Employee> getEmployees(int factoryId) throws SQLException {
		ResultSet result = getRecords("Employee", "WHERE factory="+factoryId);
		List<Employee> list = new ArrayList<>();
		while(result.next()) {
			Employee e = new Employee();
			e.setEmail(result.getString("email"));
			e.setFactory(result.getInt("factory"));
			e.setFirstName(result.getString("firstName"));
			e.setLastName(result.getString("lastName"));
			e.setPhone(result.getString("phone"));
			e.setId(result.getInt("id"));
			list.add(e);
		}
		result.close();
		return list;
	}
	
	public List<Machine> getMachines(Factory factory) throws SQLException {
		ResultSet result = getRecords("Machine", "WHERE factory="+factory.getId());
		List<Machine> list = new ArrayList<>();
		while(result.next()) {
			Machine m = new Machine();
			m.setId(result.getInt("id"));
			m.setName(result.getString("name"));
			m.setWorkingPlaces(result.getInt("workingPlaces"));
			list.add(m);
		}
		result.close();
		return list;
	}
	
	public List<FactoryMapSector> getSectors(FactoryMap factoryMap) throws SQLException {
		ResultSet result = getRecords("FactoryMapSector", "WHERE factoryMap="+factoryMap.getId());
		List<FactoryMapSector> list = new ArrayList<>();
		while(result.next()) {
			FactoryMapSector s = new FactoryMapSector();
			s.setId(result.getInt("id"));
			s.setWidth(result.getInt("width"));
			s.setHeight(result.getInt("height"));
			s.setX(result.getInt("x"));
			s.setY(result.getInt("y"));
			s.setMachine(result.getInt("machine"));
			s.setPriority(result.getInt("priority"));
			list.add(s);
		}
		result.close();
		return list;
	}
	
	public Machine getMachine(FactoryMapSector sector) throws SQLException {
		ResultSet result = getRecords("Machine", "WHERE id="+sector.getMachine());
		Machine machine = new Machine();
		machine.setId(result.getInt("id"));
		machine.setName(result.getString("name"));
		machine.setFactory(result.getInt("factory"));
		machine.setWorkingPlaces(result.getInt("workingPlaces"));
		result.close();
		return machine;
	}
	
	public List<Factory> getFactories() throws SQLException {
		ResultSet result = getRecords("Factory");
		List<Factory> list = new ArrayList<>();
		while(result.next()) {
			Factory factory = new Factory();
			factory.setId(result.getInt("id"));
			factory.setAddress(result.getString("address"));
			factory.setRestingHours(result.getInt("restingHours"));
			factory.setWorkingHours(result.getInt("workingHours"));
			list.add(factory);
		}
		result.close();
		return list;
	}
	
	public FactoryMap getFactoryMap(Factory factory) throws SQLException {
		ResultSet result = getRecords("FactoryMap", "WHERE factory="+factory.getId());
		FactoryMap factoryMap = new FactoryMap();
		factoryMap.setId(result.getInt("id"));
		factoryMap.setFactory(result.getInt("factory"));
		factoryMap.setWidth(result.getInt("width"));
		factoryMap.setHeight(result.getInt("height"));
		result.close();
		return factoryMap;
	}
	
	public Boolean chcekRequiredEntitlements(Employee employee, Machine machine) {
		String query = "select count(*) as required,"
				+ "(select count(*) "
					+ "from MachineEntitlementRequired as mer "
					+ "inner join EmployeeEntitlement as ee on ee.entitlement=mer.entitlementRequired "
					+ "where mer.machine="+machine.getId()+" and employee="+ employee.getId() +") as met "
				+ "from MachineEntitlementRequired "
				+ "where machine="+machine.getId();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			Integer required = Integer.parseInt(resultSet.getString("required"));
			Integer met = Integer.parseInt(resultSet.getString("met"));
			return (required == met);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<EmployeeEntitlement> getEmployeeEntitlements(Employee employee) throws SQLException {
		ResultSet result = getRecords("EmployeeEntitlement", "WHERE employee="+employee.getId());
		List<EmployeeEntitlement> list = new ArrayList<>();
		while(result.next()) {
			EmployeeEntitlement entitlement = new EmployeeEntitlement();
			entitlement.setId(result.getInt("id"));
			entitlement.setEmployee(result.getObject("employee"));
			entitlement.setEntitlement(result.getObject("entitlement"));
			list.add(entitlement);
		}
		result.close();
		return list;
	}
	
	public List<MachineEntitlementRequired> getMachineEntitlementRequired(Machine machine) throws SQLException {
		ResultSet result = getRecords("MachineEntitlementRequired", "WHERE machine="+machine.getId());
		List<MachineEntitlementRequired> list = new ArrayList<>();
		while(result.next()) {
			MachineEntitlementRequired entitlement = new MachineEntitlementRequired();
			entitlement.setId(result.getInt("id"));
			entitlement.setMachine(result.getInt("machine"));
			entitlement.setEntitlementRequired(result.getObject("entitlementRequired"));
			list.add(entitlement);
		}
		result.close();
		return list;
	}
	
	public List<WorkingPlace> getWorkingPlaces(FactoryMapSector sector) throws SQLException {
		ResultSet result = getRecords("WorkingPlace", "WHERE sector="+sector.getId());
		List<WorkingPlace> list = new ArrayList<>();
		while(result.next()) {
			WorkingPlace entitlement = new WorkingPlace();
			entitlement.setId(result.getInt("id"));
			entitlement.setSector(result.getInt("sector"));
			entitlement.setOccupiedBy(result.getInt("occupiedBy"));
			list.add(entitlement);
		}
		result.close();
		return list;
	}
	
}
