package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "FactoryMapSector" database table.
 * 
 */
@Entity
@Table(name="\"FactoryMapSector\"")
@NamedQuery(name="FactoryMapSector.findAll", query="SELECT f FROM FactoryMapSector f")
public class FactoryMapSector implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"factoryMap\"")
	private int factoryMap;

	@Column(name="\"height\"")
	private int height;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"machine\"")
	private int machine;

	@Column(name="\"priority\"")
	private int priority;

	@Column(name="\"width\"")
	private int width;

	@Column(name="\"x\"")
	private int x;

	@Column(name="\"y\"")
	private int y;

	public FactoryMapSector() {
	}

	public int getFactoryMap() {
		return this.factoryMap;
	}

	public void setFactoryMap(int factoryMap) {
		this.factoryMap = factoryMap;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMachine() {
		return this.machine;
	}

	public void setMachine(int machine) {
		this.machine = machine;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

}