package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Machine" database table.
 * 
 */
@Entity
@Table(name="\"Machine\"")
@NamedQuery(name="Machine.findAll", query="SELECT m FROM Machine m")
public class Machine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"factory\"")
	private int factory;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"name\"")
	private Object name;

	@Column(name="\"workingPlaces\"")
	private int workingPlaces;

	public Machine() {
	}

	public int getFactory() {
		return this.factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Object getName() {
		return this.name;
	}

	public void setName(Object name) {
		this.name = name;
	}

	public int getWorkingPlaces() {
		return this.workingPlaces;
	}

	public void setWorkingPlaces(int workingPlaces) {
		this.workingPlaces = workingPlaces;
	}

}