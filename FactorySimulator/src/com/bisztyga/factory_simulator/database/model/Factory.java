package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Factory" database table.
 * 
 */
@Entity
@Table(name="\"Factory\"")
@NamedQuery(name="Factory.findAll", query="SELECT f FROM Factory f")
public class Factory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"address\"")
	private String address;

	@Column(name="\"basicSalary\"")
	private int basicSalary;

	@Column(name="\"entitlementBonus\"")
	private int entitlementBonus;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"restingHours\"")
	private int restingHours;

	@Column(name="\"workingHours\"")
	private int workingHours;

	public Factory() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getBasicSalary() {
		return this.basicSalary;
	}

	public void setBasicSalary(int basicSalary) {
		this.basicSalary = basicSalary;
	}

	public int getEntitlementBonus() {
		return this.entitlementBonus;
	}

	public void setEntitlementBonus(int entitlementBonus) {
		this.entitlementBonus = entitlementBonus;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRestingHours() {
		return this.restingHours;
	}

	public void setRestingHours(int restingHours) {
		this.restingHours = restingHours;
	}

	public int getWorkingHours() {
		return this.workingHours;
	}

	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}

}