package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "EmployeeEntitlement" database table.
 * 
 */
@Entity
@Table(name="\"EmployeeEntitlement\"")
@NamedQuery(name="EmployeeEntitlement.findAll", query="SELECT e FROM EmployeeEntitlement e")
public class EmployeeEntitlement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"employee\"")
	private Object employee;

	@Column(name="\"entitlement\"")
	private Object entitlement;

	@Column(name="\"id\"")
	private int id;

	public EmployeeEntitlement() {
	}

	public Object getEmployee() {
		return this.employee;
	}

	public void setEmployee(Object employee) {
		this.employee = employee;
	}

	public Object getEntitlement() {
		return this.entitlement;
	}

	public void setEntitlement(Object entitlement) {
		this.entitlement = entitlement;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

}