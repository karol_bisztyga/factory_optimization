package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Entitlement" database table.
 * 
 */
@Entity
@Table(name="\"Entitlement\"")
@NamedQuery(name="Entitlement.findAll", query="SELECT e FROM Entitlement e")
public class Entitlement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"name\"")
	private Object name;

	public Entitlement() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Object getName() {
		return this.name;
	}

	public void setName(Object name) {
		this.name = name;
	}

}