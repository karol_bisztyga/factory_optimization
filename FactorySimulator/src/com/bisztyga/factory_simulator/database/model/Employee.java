package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Employee" database table.
 * 
 */
@Entity
@Table(name="\"Employee\"")
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"email\"")
	private Object email;

	@Column(name="\"factory\"")
	private int factory;

	@Column(name="\"firstName\"")
	private Object firstName;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"lastName\"")
	private Object lastName;

	@Column(name="\"phone\"")
	private Object phone;

	public Employee() {
	}

	public Object getEmail() {
		return this.email;
	}

	public void setEmail(Object email) {
		this.email = email;
	}

	public int getFactory() {
		return this.factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}

	public Object getFirstName() {
		return this.firstName;
	}

	public void setFirstName(Object firstName) {
		this.firstName = firstName;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Object getLastName() {
		return this.lastName;
	}

	public void setLastName(Object lastName) {
		this.lastName = lastName;
	}

	public Object getPhone() {
		return this.phone;
	}

	public void setPhone(Object phone) {
		this.phone = phone;
	}

}