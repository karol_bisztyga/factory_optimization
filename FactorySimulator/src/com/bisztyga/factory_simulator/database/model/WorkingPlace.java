package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "WorkingPlace" database table.
 * 
 */
@Entity
@Table(name="\"WorkingPlace\"")
@NamedQuery(name="WorkingPlace.findAll", query="SELECT w FROM WorkingPlace w")
public class WorkingPlace implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"occupiedBy\"")
	private int occupiedBy;

	@Column(name="\"sector\"")
	private Object sector;

	public WorkingPlace() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOccupiedBy() {
		return this.occupiedBy;
	}

	public void setOccupiedBy(int occupiedBy) {
		this.occupiedBy = occupiedBy;
	}

	public Object getSector() {
		return this.sector;
	}

	public void setSector(Object sector) {
		this.sector = sector;
	}

}