package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "FactoryMap" database table.
 * 
 */
@Entity
@Table(name="\"FactoryMap\"")
@NamedQuery(name="FactoryMap.findAll", query="SELECT f FROM FactoryMap f")
public class FactoryMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"factory\"")
	private int factory;

	@Column(name="\"height\"")
	private int height;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"width\"")
	private int width;

	public FactoryMap() {
	}

	public int getFactory() {
		return this.factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

}