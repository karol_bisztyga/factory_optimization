package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Report" database table.
 * 
 */
@Entity
@Table(name="\"Report\"")
@NamedQuery(name="Report.findAll", query="SELECT r FROM Report r")
public class Report implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"date\"")
	private Object date;

	@Column(name="\"employee\"")
	private int employee;

	@Column(name="\"factory\"")
	private int factory;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"period\"")
	private int period;

	@Column(name="\"sector\"")
	private int sector;

	public Report() {
	}

	public Object getDate() {
		return this.date;
	}

	public void setDate(Object date) {
		this.date = date;
	}

	public int getEmployee() {
		return this.employee;
	}

	public void setEmployee(int employee) {
		this.employee = employee;
	}

	public int getFactory() {
		return this.factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPeriod() {
		return this.period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public int getSector() {
		return this.sector;
	}

	public void setSector(int sector) {
		this.sector = sector;
	}

}