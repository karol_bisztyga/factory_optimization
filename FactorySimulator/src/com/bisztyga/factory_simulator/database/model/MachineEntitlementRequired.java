package com.bisztyga.factory_simulator.database.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "MachineEntitlementRequired" database table.
 * 
 */
@Entity
@Table(name="\"MachineEntitlementRequired\"")
@NamedQuery(name="MachineEntitlementRequired.findAll", query="SELECT m FROM MachineEntitlementRequired m")
public class MachineEntitlementRequired implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"entitlementRequired\"")
	private Object entitlementRequired;

	@Column(name="\"id\"")
	private int id;

	@Column(name="\"machine\"")
	private int machine;

	public MachineEntitlementRequired() {
	}

	public Object getEntitlementRequired() {
		return this.entitlementRequired;
	}

	public void setEntitlementRequired(Object entitlementRequired) {
		this.entitlementRequired = entitlementRequired;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMachine() {
		return this.machine;
	}

	public void setMachine(int machine) {
		this.machine = machine;
	}

}