package com.bisztyga.factory_simulator.app;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.communication.sockets.WebsocketServer;
import com.bisztyga.factory_simulator.database.DatabaseManager;
import com.bisztyga.factory_simulator.database.model.Factory;

public class MainController {
	
	private static MainController instance;
	private final Logger logger;
	private final DatabaseManager databaseManager;
	private final ExecutorService executorService = Executors.newCachedThreadPool();
	private final List<FactoryAgent> activeFactories = new ArrayList<>();
	private final WebsocketServer socketServer; 

	private MainController() throws SecurityException, IOException {
		databaseManager = new DatabaseManager();
		logger = Logger.getLogger("FactorySimulatorLogger");
		String path = System.getProperty("user.dir") + File.separator + "logs";
		new File(path).mkdirs();
		FileHandler fileHandler = new FileHandler(path + File.separator + "logs.txt");
		logger.addHandler(fileHandler);
		fileHandler.setFormatter(new SimpleFormatter());
		socketServer = new WebsocketServer();
	}

	public static MainController getInstance() {
		if(instance == null) {
			try {
				instance = new MainController();
			} catch (SecurityException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		return instance;
	}

	public static void log(String info) {
		MainController.getInstance().logger.info(info);
	}
	
	public static void logError(String info) {
		log("[ERROR] " + info);
	}
	
	public void appendQueryForAnalysis(String date, String query, FactoryAgent factory) {
		try {
			String path = System.getProperty("user.dir") + File.separator + ".." + File.separator + "mutualFiles" + 
					File.separator + "databaseScripts";
			new File(path).mkdirs();
			path = path + File.separator + "queries_"+date+"_F"+factory.getFactoryModel().getId()+".sql";
			File file = new File(path);
			file.createNewFile();
			query += ";";
			Files.write(Paths.get(path), query.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public DatabaseManager getDatabaseManager() {
		return databaseManager;
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}
	
	public void launchFactory(FactoryAgent factory) {
		executorService.execute(factory);
		if(activeFactories.isEmpty()) {
			MainController.getInstance().getExecutorService().execute(socketServer);
		}
		activeFactories.add(factory);
	}
	
	public void closeFactory(FactoryAgent factory) {
		factory.getExecutorService().shutdownNow();
		activeFactories.remove(factory);
		if(activeFactories.isEmpty()) {
			executorService.shutdownNow();
			try {
				socketServer.stop();
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public WebsocketServer getSocketServer() {
		return socketServer;
	}

	public static void main(String[] args) {
		try {
			List<Factory> factories = MainController.getInstance().databaseManager.getFactories();
			
			for(Factory factory : factories) {
				MainController.log("starting agent for " + factory.getId() + "/" + factory.getAddress() + "/" + 
						factory.getWorkingHours() + "/" + factory.getRestingHours());
				
				FactoryAgent factoryAgent = new FactoryAgent(factory, MainController.getInstance().getSocketServer());
				MainController.getInstance().launchFactory(factoryAgent);
				MainController.getInstance().getSocketServer().addFactoryAgent(factoryAgent);
			}
			MainController.getInstance().getExecutorService().shutdown();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
