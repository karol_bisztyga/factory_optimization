package com.bisztyga.factory_simulator.agents.simulations;

import java.util.concurrent.TimeUnit;

public class DelayedActionPerformer implements Runnable {
	
	private Thread thread;
	private final long timeout;

	public DelayedActionPerformer(Runnable action, long timeout) {
		thread = new Thread(action);
		this.timeout = timeout;
	}
	
	@Override
	public void run() {
		try {
			TimeUnit.MILLISECONDS.sleep(timeout);
			thread.start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
