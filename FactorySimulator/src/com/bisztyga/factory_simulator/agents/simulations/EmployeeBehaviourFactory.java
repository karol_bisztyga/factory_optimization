package com.bisztyga.factory_simulator.agents.simulations;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;

public class EmployeeBehaviourFactory {

	public static EmployeeBehaviour getEmployeeBehaviour(EmployeeAgent employeeAgent) {
		int rnd = EmployeeBehaviour.random.nextInt(3);
		switch(rnd) {
			case 0:
				return new LaidBackEmployeeBehaviour(employeeAgent);
			case 1:
				return new StrictEmployeeBehaviour(employeeAgent);
			default:
				return new AverageEmployeeBehaviour(employeeAgent);
		}
	}

}
