package com.bisztyga.factory_simulator.agents.simulations;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;

public class LaidBackEmployeeBehaviour implements EmployeeBehaviour {

	private final EmployeeAgent employeeAgent;
	
	public LaidBackEmployeeBehaviour(EmployeeAgent employeeAgent) {
		super();
		this.employeeAgent = employeeAgent;
	}

	@Override
	public void beLateAtWork() {
		Thread th = new Thread(new DelayedActionPerformer(new Runnable() {
			@Override
			public void run() {
				employeeAgent.setState(STATE.READY);
				scheduleNextBreak();
			}
		}, getLateForWorkTimeout()));
		th.start();
	}

	@Override
	public boolean makeTaskDecision(SectorTool sector) {
		if(EmployeeBehaviour.random.nextInt(100) < getPercentageChanceForGroundlessTaskRejection()) {
			return false;
		}
		int freeWorkingPlaces = sector.getFreeWorkingPlaces();
		if(employeeAgent.getState() == STATE.READY && freeWorkingPlaces > 0) {
			return true;
		}
		return false;
	}

	@Override
	public void scheduleNextBreak() {
		scheduleNextBreak(getScheduleBreakTimeout());
	}
	
	public void scheduleNextBreak(int customTimeout) {
		Thread th = new Thread(new DelayedActionPerformer(new Runnable() {
			@Override
			public void run() {
				try {
					employeeAgent.askForABreak();
				} catch (InterruptedException e) {
					e.printStackTrace();
					scheduleNextBreak(customTimeout);
				}
			}
		}, customTimeout));
		th.start();
	}

	@Override
	public void handleBreakDecision(boolean decision) {
		if(decision) {
			Thread th = new Thread(new DelayedActionPerformer(new Runnable() {
				@Override
				public void run() {
					employeeAgent.comeBackFromBreak();
				}
			}, employeeAgent.getOwnerFactoryAgent().getRestingMinutes()/4));
			th.start();
		} else {
			scheduleNextBreak(getScheduleBreakTimeout()/3);
		}
	}

	@Override
	public String getLabel() {
		return "laid back";
	}

	@Override
	public int getScheduleBreakTimeout() {
		return employeeAgent.getOwnerFactoryAgent().getWorkingMinutes()/4;
	}

	@Override
	public int getLateForWorkTimeout() {
		return EmployeeBehaviour.random.nextInt(employeeAgent.getOwnerFactoryAgent().getWorkingMinutes()/6);
	}

	@Override
	public int getPercentageChanceForGroundlessTaskRejection() {
		return 30;
	}

}
