package com.bisztyga.factory_simulator.agents.simulations;

import java.util.Random;

import com.bisztyga.factory_simulator.agents.tools.SectorTool;

public interface EmployeeBehaviour {
	
	public static Random random = new Random();
	
	public void beLateAtWork();
	public boolean makeTaskDecision(SectorTool sector);
	public void scheduleNextBreak();
	public void handleBreakDecision(boolean decision);
	public String getLabel();
	public int getScheduleBreakTimeout();
	public int getLateForWorkTimeout();
	public int getPercentageChanceForGroundlessTaskRejection();
	
}
