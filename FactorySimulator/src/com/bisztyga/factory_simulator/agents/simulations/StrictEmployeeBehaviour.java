package com.bisztyga.factory_simulator.agents.simulations;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;

public class StrictEmployeeBehaviour implements EmployeeBehaviour {

	private final EmployeeAgent employeeAgent;
	
	public StrictEmployeeBehaviour(EmployeeAgent employeeAgent) {
		super();
		this.employeeAgent = employeeAgent;
	}

	@Override
	public void beLateAtWork() {
		employeeAgent.setState(STATE.READY);
		scheduleNextBreak();
	}

	@Override
	public boolean makeTaskDecision(SectorTool sector) {
		if(EmployeeBehaviour.random.nextInt(100) < getPercentageChanceForGroundlessTaskRejection()) {
			return false;
		}
		int freeWorkingPlaces = sector.getFreeWorkingPlaces();
		if(employeeAgent.getState() == STATE.READY && freeWorkingPlaces > 0) {
			return true;
		}
		return false;
	}

	public void scheduleNextBreak(int customTimeout) {
		Thread th = new Thread(new DelayedActionPerformer(new Runnable() {
			@Override
			public void run() {
				try {
					employeeAgent.askForABreak();
				} catch (InterruptedException e) {
					e.printStackTrace();
					scheduleNextBreak(customTimeout);
				}
			}
		}, customTimeout));
		th.start();
	}
	
	@Override
	public void scheduleNextBreak() {
		scheduleNextBreak(getScheduleBreakTimeout());
	}

	@Override
	public void handleBreakDecision(boolean decision) {
		if(decision) {
			Thread th = new Thread(new DelayedActionPerformer(new Runnable() {
				@Override
				public void run() {
					employeeAgent.comeBackFromBreak();
				}
			}, employeeAgent.getOwnerFactoryAgent().getRestingMinutes()/2));
			th.start();
		} else {
			scheduleNextBreak(getScheduleBreakTimeout()/3);
		}
	}

	@Override
	public String getLabel() {
		return "strict";
	}

	@Override
	public int getScheduleBreakTimeout() {
		return employeeAgent.getOwnerFactoryAgent().getWorkingMinutes()/8;
	}

	@Override
	public int getLateForWorkTimeout() {
		return 0;
	}

	@Override
	public int getPercentageChanceForGroundlessTaskRejection() {
		return 5;
	}
	
}
