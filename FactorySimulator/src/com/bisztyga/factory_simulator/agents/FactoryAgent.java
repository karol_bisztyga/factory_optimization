package com.bisztyga.factory_simulator.agents;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.agents.communication.Message;
import com.bisztyga.factory_simulator.agents.communication.Message.CONTENT_CODE;
import com.bisztyga.factory_simulator.agents.communication.Message.TYPE;
import com.bisztyga.factory_simulator.agents.communication.Response;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;
import com.bisztyga.factory_simulator.agents.tools.compute.WorkflowCalculator;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.communication.sockets.WebsocketServer;
import com.bisztyga.factory_simulator.communication.sockets.tools.WorkingData;
import com.bisztyga.factory_simulator.database.DatabaseManager;
import com.bisztyga.factory_simulator.database.model.Employee;
import com.bisztyga.factory_simulator.database.model.Factory;
import com.bisztyga.factory_simulator.database.model.FactoryMap;
import com.bisztyga.factory_simulator.database.model.FactoryMapSector;
import com.bisztyga.factory_simulator.database.model.Machine;
import com.bisztyga.factory_simulator.exceptions.MessageTypeException;
import com.bisztyga.factory_simulator.reporting.agents.AgentReportTool;
import com.bisztyga.factory_simulator.tools.Clock;

public class FactoryAgent implements Agent {

	private final Factory factoryModel;
	private final List<SectorTool> sectorTools;
	private final ExecutorService executorService;
	private final Clock clock;
	private final WebsocketServer socketServer;
	private FactoryMap factoryMap;
	private BlockingQueue<Message> messageQueue = new LinkedBlockingDeque<>();
	private final WorkflowCalculator workflowCalculator;
	private final WorkingData workingData;
	private AgentReportTool agentReportTool;
	private final List<EmployeeAgent> employeeAgents;
	
	public FactoryAgent(Factory factoryModel, WebsocketServer socketServer) {
		this.factoryModel = factoryModel;
		this.sectorTools = new ArrayList<>();
		executorService = Executors.newCachedThreadPool();
		clock = new Clock(factoryModel.getWorkingHours()*60, this);
		this.socketServer = socketServer;
		workflowCalculator = new WorkflowCalculator(this);
		workingData = new WorkingData();
		employeeAgents = new ArrayList<>();
	}

	@Override
	public void run() {
		try {
			MainController.log("["+ this +"] start");
			executorService.execute(getWorkflowCalculator().getEmployeeAssigner());
			DatabaseManager dbm = MainController.getInstance().getDatabaseManager();
			
			List<Employee> employees = dbm.getEmployees(factoryModel.getId());
			for(Employee employee : employees) {
				EmployeeAgent employeeAgent = new EmployeeAgent(employee, this);
				getWorkingData().addEmployee(employeeAgent.getEmployee().getId());
				employeeAgents.add(employeeAgent);
			}
			
			this.factoryMap = dbm.getFactoryMap(factoryModel);
			List<FactoryMapSector> sectors = dbm.getSectors(factoryMap);
			for(FactoryMapSector sector : sectors) {
				Machine machine = dbm.getMachine(sector);
				SectorTool sectorTool = new SectorTool(sector, machine, this);
				sectorTools.add(sectorTool);
			}
			
			this.agentReportTool = new AgentReportTool(this);
			
			executorService.execute(clock);
			
			for(EmployeeAgent employeeAgent : getEmployeeAgents()) {
				executorService.execute(employeeAgent);
			}
			
			MainController.log("[factory " + factoryModel.getId() + "] start");
			
			executorService.shutdown();
			
			while(true) {
				try {
					waitForMessage();
					Message currentMessage = messageQueue.take();
					switch (currentMessage.getCategory()) {
					case REQUEST:
						switch(currentMessage.getType()) {
							case BREAK:
								EmployeeAgent employee = (EmployeeAgent)currentMessage.getSender();
								boolean allow = workflowCalculator.allowBreakDecide(employee);
								//decide if the employee is allowed to go to break...
								Map<Message.CONTENT_CODE, Object> content = new HashMap<>();
								content.put(CONTENT_CODE.BREAK_DECISION, allow);
								employee.receiveMessage(new Response(content, TYPE.BREAK, this));
								break;
							default:
								throw new MessageTypeException(currentMessage.getType());
						}
						break;
					case RESPONSE:
						switch(currentMessage.getType()) {
							case EMPLOYEE_NEW_WORKPLACE:
								SectorTool sector = (SectorTool) currentMessage.getContent().get(CONTENT_CODE.SECTOR_S);
								EmployeeAgent employee = (EmployeeAgent)currentMessage.getSender();
								if(sector == null) {
									getWorkflowCalculator().getEmployeeAssigner().assign(employee);
								} else {
									if(!sector.attachEmployee(employee)) {
										getWorkflowCalculator().getEmployeeAssigner().assign(employee);
										break;
									}
									getWorkingData().employeeNewWorkplace(
											employee.getEmployee().getId(), 
											sector.getSector().getId());
									employee.setState(STATE.WORKING);
								}
								break;
							default:
								throw new MessageTypeException(currentMessage.getType());
						}
						break;
					}
				} catch (MessageTypeException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					break;
				}
			}
			MainController.log("["+ this +"] end");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void endDayOfWork() {
		getSocketServer().sendEndOfTheDayToAll(this);
    	MainController.log("the day of work has ended for factory " + getFactoryModel().getAddress());
		for(int i=0 ; i<getEmployeeAgents().size() ; ++i) {
			getEmployeeAgents().get(i).setState(STATE.NOT_PRESENT);
		}
		agentReportTool.report();
		MainController.getInstance().closeFactory(this);
	}

	public List<EmployeeAgent> getEmployeeAgents() {
		return employeeAgents;
	}

	public EmployeeAgent getEmployeeAgent(int employeeId) {
		for(EmployeeAgent employeeAgent : getEmployeeAgents()) {
			if(employeeAgent.getEmployee().getId() == employeeId) {
				return employeeAgent;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "FactoryAgent [factoryModel=" + factoryModel.getAddress() + "]";
	}

	public List<SectorTool> getMachineAgents() {
		return sectorTools;
	}

	public Clock getClock() {
		return clock;
	}

	public synchronized WebsocketServer getSocketServer() {
		return socketServer;
	}

	public Factory getFactoryModel() {
		return factoryModel;
	}

	public synchronized List<SectorTool> getSectorTools() {
		return sectorTools;
	}
	
	
	public ExecutorService getExecutorService() {
		return executorService;
	}

	public List<SectorTool> getSectorToolsWithEmptyPlaces() {
		List<SectorTool> result = new ArrayList<>();
		for(SectorTool sector : getSectorTools()) {
			if(sector.getFreeWorkingPlaces() > 0) {
				result.add(sector);
			}
		}
		return result;
	}
	
	public SectorTool getSectorTool(int sectorId) {
		for(SectorTool sectorTool : sectorTools) {
			if(sectorTool.getSector().getId() == sectorId) {
				return sectorTool;
			}
		}
		return null;
	}
	
	public int getWorkingMinutes() {
		return factoryModel.getWorkingHours()*60*Clock.MINUTE_PERIOD_MS;
	}
	
	public int getRestingMinutes() {
		return factoryModel.getRestingHours()*60*Clock.MINUTE_PERIOD_MS;
	}

	public WorkflowCalculator getWorkflowCalculator() {
		return workflowCalculator;
	}

	public WorkingData getWorkingData() {
		return workingData;
	}
	

	public AgentReportTool getAgentReportTool() {
		return agentReportTool;
	}
	
	@Override
	public synchronized void waitForMessage() throws InterruptedException {
		while(messageQueue.isEmpty()) {
			wait();
		}
	}

	@Override
	public synchronized void receiveMessage(Message message) throws InterruptedException {
		messageQueue.put(message);
		notifyAll();
	}

	@Override
	public synchronized void sendMessage(Message message, Agent receiver) throws InterruptedException {
		receiver.receiveMessage(message);
	}
	
	
}
