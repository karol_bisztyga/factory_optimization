package com.bisztyga.factory_simulator.agents.tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.agents.communication.Message;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.database.DatabaseManager;
import com.bisztyga.factory_simulator.database.model.Employee;
import com.bisztyga.factory_simulator.database.model.FactoryMapSector;
import com.bisztyga.factory_simulator.database.model.Machine;
import com.bisztyga.factory_simulator.database.model.MachineEntitlementRequired;
import com.bisztyga.factory_simulator.database.model.WorkingPlace;
import com.bisztyga.factory_simulator.exceptions.MessageTypeException;

public class SectorTool {

	private final Machine machine;
	private final FactoryMapSector sector;
	private final List<EmployeeAgent> allEmployees;
	private final FactoryAgent ownerFactoryAgent;
	private List<MachineEntitlementRequired> entitlementsRequired;
	private List<WorkingPlace> workingPlaces;
	private final ExecutorService executorService;
	
	public SectorTool(
			FactoryMapSector sector, 
			Machine machine, 
			FactoryAgent ownerFactoryAgent) {
		this.machine = machine;
		this.sector = sector;
		this.ownerFactoryAgent = ownerFactoryAgent;
		executorService = Executors.newCachedThreadPool();
		allEmployees = new ArrayList<>();
		try {
			this.workingPlaces = new ArrayList<>(MainController.getInstance().getDatabaseManager().getWorkingPlaces(sector));
			this.entitlementsRequired = MainController.getInstance().getDatabaseManager().getMachineEntitlementRequired(machine);
		} catch (SQLException e) {
			e.printStackTrace();
			this.entitlementsRequired = null;
			this.workingPlaces = null;
		}
	}
	
	public int getFreeWorkingPlaces() {
		int employeesCountInSector = getOwnerFactoryAgent().getWorkingData().getEmployeesCountInSector(getSector().getId());
		return getMachine().getWorkingPlaces() - employeesCountInSector;
	}
	
	public boolean attachEmployee(EmployeeAgent employee) {
		if(getFreeWorkingPlaces() <= 0) {
			return false;
		}
		getOwnerFactoryAgent().getWorkingData().employeeNewWorkplace(employee.getEmployee().getId(), getSector().getId());
		return true;
	}
	
	public boolean unattachEmployee(EmployeeAgent employee) {
		if(getOwnerFactoryAgent().getWorkingData().isEmployeeInSector(employee.getEmployee().getId(), getSector().getId())) {
			return false;
		}
		getOwnerFactoryAgent().getWorkingData().employeeNewWorkplace(employee.getEmployee().getId(), 0);
		return true;
	}
	
	public Machine getMachine() {
		return machine;
	}

	public FactoryMapSector getSector() {
		return sector;
	}

	public synchronized List<WorkingPlace> getWorkingPlaces() {
		return workingPlaces;
	}

	public List<EmployeeAgent> getAllEmployees() {
		return allEmployees;
	}

	public List<MachineEntitlementRequired> getEntitlementsReuired() {
		return entitlementsRequired;
	}

	public FactoryAgent getOwnerFactoryAgent() {
		return ownerFactoryAgent;
	}

	@Override
	public String toString() {
		return "SectorTool [machine=" + machine.getName() + ", sector=" + sector.getId() + "]";
	}
}
