package com.bisztyga.factory_simulator.agents.tools;

import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.exceptions.EmployeeStateException;

public class EmployeeStateChecker {
	
	public static void check(STATE oldState, STATE newState) throws EmployeeStateException {
		if(oldState == newState && newState != STATE.WORKING) {
			throw new EmployeeStateException("illegal state change from " + oldState + " to " + newState);
		}
		switch(oldState) {
		case BREAK:
			if(newState == STATE.WORKING)
				throw new EmployeeStateException("illegal state change from " + oldState + " to " + newState);
			break;
		case NOT_PRESENT:
			if(newState != STATE.READY)
				throw new EmployeeStateException("illegal state change from " + oldState + " to " + newState);
			break;
		case READY:
			break;
		case WORKING:
			if(newState == STATE.READY)
				throw new EmployeeStateException("illegal state change from " + oldState + " to " + newState);
			break;
		default:
			throw new EmployeeStateException("unrecognized state " + oldState);
		}
	}
	
}
