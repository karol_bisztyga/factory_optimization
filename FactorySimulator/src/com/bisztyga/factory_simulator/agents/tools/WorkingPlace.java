package com.bisztyga.factory_simulator.agents.tools;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;

public class WorkingPlace {
	
	private EmployeeAgent employeeAgent;
	private final SectorTool sectorTool;
	
	public WorkingPlace(EmployeeAgent employeeAgent, SectorTool sectorTool) {
		this.employeeAgent = employeeAgent;
		this.sectorTool = sectorTool;
	}

	public EmployeeAgent getEmployeeAgent() {
		return employeeAgent;
	}

	public void setEmployeeAgent(EmployeeAgent employeeAgent) {
		this.employeeAgent = employeeAgent;
	}

	public SectorTool getSectorTool() {
		return sectorTool;
	}

	@Override
	public String toString() {
		return "WorkingPlace [employeeAgent=" + employeeAgent + ", sectorTool=" + sectorTool + "]";
	}
	
	
}
