package com.bisztyga.factory_simulator.agents.tools.compute;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.agents.communication.Message.CONTENT_CODE;
import com.bisztyga.factory_simulator.agents.communication.Message.TYPE;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;
import com.bisztyga.factory_simulator.agents.communication.Request;

public class EmployeeAssigner implements Runnable {

	private final FactoryAgent ownerFactoryAgent;
	private final BlockingQueue<EmployeeAgent> employeesToCalculateQueue = new LinkedBlockingDeque<>();
	
	public EmployeeAssigner(FactoryAgent ownerFactoryAgent) {
		this.ownerFactoryAgent = ownerFactoryAgent;
	}


	@Override
	public void run() {
		while(true) {
			try {
				waitForEmployee();
				EmployeeAgent employee = employeesToCalculateQueue.take();
				
				List<SectorTool> sectorsWithEmptyPlaces = getOwnerFactoryAgent().getSectorToolsWithEmptyPlaces();
				for(int i=0 ; i<sectorsWithEmptyPlaces.size() ; ++i) {
					SectorTool sector = sectorsWithEmptyPlaces.get(i);
					if(!getOwnerFactoryAgent().getWorkflowCalculator().checkIfEmployeePassesRequirements(employee, sector)) {
						sectorsWithEmptyPlaces.remove(sector);
					}
				}
				sectorsWithEmptyPlaces.sort(new SectorToolComparator());
				Map<CONTENT_CODE, Object> content = new HashMap<>();
				content.put(CONTENT_CODE.SECTOR_S, sectorsWithEmptyPlaces);
				employee.receiveMessage(new Request(content, TYPE.EMPLOYEE_NEW_WORKPLACE, getOwnerFactoryAgent()));
				
			} catch (InterruptedException e) {
				break;
			}
		}
	}
	
	private synchronized void waitForEmployee() throws InterruptedException {
		while(employeesToCalculateQueue.isEmpty()) {
			wait();
		}
	}
	
	public synchronized void assign(EmployeeAgent employee) throws InterruptedException {
		employeesToCalculateQueue.put(employee);
		notifyAll();
	}

	public FactoryAgent getOwnerFactoryAgent() {
		return ownerFactoryAgent;
	}
	
	private static class SectorToolComparator implements Comparator<SectorTool> {
		@Override
		public int compare(SectorTool s1, SectorTool s2) {
			int result = Integer.compare(s2.getSector().getPriority(), s1.getSector().getPriority());
			//if the priority is the same, take one with more entitlements required
			if(result == 0) {
				result = Integer.compare(s2.getEntitlementsReuired().size(), s1.getEntitlementsReuired().size());
			}
			return result;
		}
	}

}
