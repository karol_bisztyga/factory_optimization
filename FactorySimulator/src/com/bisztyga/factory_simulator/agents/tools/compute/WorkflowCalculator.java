package com.bisztyga.factory_simulator.agents.tools.compute;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.bisztyga.factory_simulator.agents.EmployeeAgent;
import com.bisztyga.factory_simulator.agents.EmployeeAgent.STATE;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;
import com.bisztyga.factory_simulator.agents.FactoryAgent;
import com.bisztyga.factory_simulator.database.model.EmployeeEntitlement;

public class WorkflowCalculator {
	
	private final FactoryAgent ownerFactoryAgent;
	private final EmployeeAssigner employeeAssigner;
	private BlockingQueue<EmployeeAgent> breakQueue = new LinkedBlockingDeque<>();

	public WorkflowCalculator(FactoryAgent ownerFactoryAgent) {
		this.ownerFactoryAgent = ownerFactoryAgent;
		employeeAssigner = new EmployeeAssigner(ownerFactoryAgent);
	}	
	
	public boolean allowBreakDecide(EmployeeAgent employee) throws InterruptedException {
		if(!breakQueue.contains(employee)) {
			breakQueue.put(employee);
		}
		int breakingEmployees = 0;
		for(int i=0 ; i<ownerFactoryAgent.getEmployeeAgents().size() ; ++i) {
			if(ownerFactoryAgent.getEmployeeAgents().get(i).getState() == STATE.BREAK) ++breakingEmployees;
		}
		boolean result = (breakingEmployees < ownerFactoryAgent.getEmployeeAgents().size()/4);
		if(result) {
			if(breakQueue.peek() != null) {
				EmployeeAgent firstEmployeeToGo = (EmployeeAgent)breakQueue.peek();
				if(firstEmployeeToGo == employee) {
					breakQueue.take();
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean checkIfEmployeePassesRequirements(EmployeeAgent employee, SectorTool sector) {
		Set<Object> requiredEntitlementsSet = new HashSet<>();
		for(int i=0 ; i<sector.getEntitlementsReuired().size() ; ++i) {
			requiredEntitlementsSet.add(
					sector.getEntitlementsReuired().get(i).getEntitlementRequired());
		}
		
		List<EmployeeEntitlement> employeeEntitlements = employee.getEmployeeEntitlements();
		Set<Object> employeeEntitlementsSet = new HashSet<>();
		for(int j=0 ; j<employeeEntitlements.size() ; ++j) {
			employeeEntitlementsSet.add(employeeEntitlements.get(j).getEntitlement());
		}
		Set<Object> requiredEntitlementsSetCopy = new HashSet<>(requiredEntitlementsSet);
		requiredEntitlementsSetCopy.removeAll(employeeEntitlementsSet);
		return (requiredEntitlementsSetCopy.isEmpty());
	}


	public EmployeeAssigner getEmployeeAssigner() {
		return employeeAssigner;
	}
	
}
