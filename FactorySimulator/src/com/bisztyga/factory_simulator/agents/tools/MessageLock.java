package com.bisztyga.factory_simulator.agents.tools;

public class MessageLock {
	
	private boolean isLocked = false;
	
	public synchronized void lock() {
		try {
			while(isLocked){
				wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		isLocked = true;
	}
	
	public synchronized void unlock(){
		isLocked = false;
		notifyAll();
	}
}
