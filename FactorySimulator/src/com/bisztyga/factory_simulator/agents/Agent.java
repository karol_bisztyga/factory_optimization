package com.bisztyga.factory_simulator.agents;

import com.bisztyga.factory_simulator.agents.communication.Message;

public interface Agent extends Runnable {
	
	public void waitForMessage() throws InterruptedException ;
	public void receiveMessage(Message message) throws InterruptedException;
	public void sendMessage(Message message, Agent receiver) throws InterruptedException;
	
}
