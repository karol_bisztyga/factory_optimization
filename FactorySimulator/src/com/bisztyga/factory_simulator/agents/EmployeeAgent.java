package com.bisztyga.factory_simulator.agents;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import com.bisztyga.factory_simulator.agents.communication.Message;
import com.bisztyga.factory_simulator.agents.communication.Message.CONTENT_CODE;
import com.bisztyga.factory_simulator.agents.communication.Message.TYPE;
import com.bisztyga.factory_simulator.agents.communication.Request;
import com.bisztyga.factory_simulator.agents.communication.Response;
import com.bisztyga.factory_simulator.agents.simulations.EmployeeBehaviour;
import com.bisztyga.factory_simulator.agents.simulations.EmployeeBehaviourFactory;
import com.bisztyga.factory_simulator.agents.tools.EmployeeStateChecker;
import com.bisztyga.factory_simulator.agents.tools.SectorTool;
import com.bisztyga.factory_simulator.app.MainController;
import com.bisztyga.factory_simulator.communication.sockets.tools.wrappers.NewState;
import com.bisztyga.factory_simulator.database.model.Employee;
import com.bisztyga.factory_simulator.database.model.EmployeeEntitlement;
import com.bisztyga.factory_simulator.exceptions.EmployeeStateException;
import com.bisztyga.factory_simulator.exceptions.MessageTypeException;
import com.bisztyga.factory_simulator.exceptions.ReportException;

public class EmployeeAgent implements Agent {

	public enum STATE {
		READY(0),
		WORKING(1),
		BREAK(2),
		NOT_PRESENT(3);
		
		private final int value;
		private STATE(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	
	private final Employee employee;
	private final FactoryAgent ownerFactoryAgent;
	private STATE state = STATE.NOT_PRESENT;
	private BlockingQueue<Message> messageQueue = new LinkedBlockingDeque<>();
	private List<EmployeeEntitlement> employeeEntitlements;
	private final EmployeeBehaviour behaviour;
	
	public EmployeeAgent(Employee employee, FactoryAgent ownerFactoryAgent) {
		this.employee = employee;
		this.ownerFactoryAgent = ownerFactoryAgent;
		behaviour = EmployeeBehaviourFactory.getEmployeeBehaviour(this);
		try {
			employeeEntitlements = MainController.getInstance().getDatabaseManager()
					.getEmployeeEntitlements(employee);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		MainController.log("["+ this +"] start");
		behaviour.beLateAtWork();
		while(true) {
			try {
				waitForMessage();
				Message currentMessage = messageQueue.take();
				switch(currentMessage.getCategory()) {
				case REQUEST:
					switch(currentMessage.getType()) {
						case EMPLOYEE_NEW_WORKPLACE:
							@SuppressWarnings("unchecked") List<SectorTool> sectors = 
									(List<SectorTool>) currentMessage.getContent().get(CONTENT_CODE.SECTOR_S);
							sectors = new ArrayList<>(sectors);
							SectorTool sectorChosen = null;
							for(int i=0 ; i<sectors.size() ; ++i) {
								boolean decision = behaviour.makeTaskDecision(sectors.get(i));
								if(decision) {
									sectorChosen = sectors.get(i);
									break;
								}
							}
							Map<CONTENT_CODE, Object> content = new HashMap<>();
							content.put(CONTENT_CODE.SECTOR_S, sectorChosen);
							currentMessage.getSender().receiveMessage(new Response(
									content, 
									TYPE.EMPLOYEE_NEW_WORKPLACE, 
									this));
							break;
						default:
							throw new MessageTypeException(currentMessage.getType());
					}
					break;
				case RESPONSE:
					switch(currentMessage.getType()) {
						case BREAK:
							boolean decision = (boolean)currentMessage.getContent().
									get(Message.CONTENT_CODE.BREAK_DECISION);
							if(decision) {
								goForABreak();
							}
							behaviour.handleBreakDecision(decision);
							break;
						default:
							throw new MessageTypeException(currentMessage.getType());
					}
					break;
			}
			} catch (MessageTypeException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				break;
			}
		}
		MainController.log("["+ this +"] end");
	}
	
	public void goForABreak() {
		setState(STATE.BREAK);
	}
	
	public void comeBackFromBreak() {
		setState(STATE.READY);
	}

	public void askForABreak() throws InterruptedException {
		ownerFactoryAgent.receiveMessage(new Request(null, TYPE.BREAK, this));
	}

	public void setState(STATE state) {
		STATE oldState = this.state;
		try {
			EmployeeStateChecker.check(this.state, state);
		} catch (EmployeeStateException e) {
			MainController.log(this + ": " + e.getMessage());
			return;
		}
		this.state = state;
		int sectorId = 0;
		if(state == STATE.WORKING) {
			try {
				sectorId = ownerFactoryAgent.getWorkingData().getEmployeeSector(getEmployee().getId());
			} catch (Exception e) {
				sectorId = 0;
			}
			try {
				if(sectorId != 0) {
					SectorTool sector = getOwnerFactoryAgent().getSectorTool(sectorId);
					getOwnerFactoryAgent().getAgentReportTool().startWork(sector, this);
				}
			} catch (ReportException e) {
				e.printStackTrace();
			}
		} else if(oldState == STATE.WORKING) {
			try {
				getOwnerFactoryAgent().getAgentReportTool().stopWork(this);
			} catch (ReportException e) {
				e.printStackTrace();
			}
		}
		getOwnerFactoryAgent().getWorkingData().employeeNewWorkplace(employee.getId(), sectorId);
		SectorTool sector = (sectorId==0) ? null : getOwnerFactoryAgent().getSectorTool(sectorId);
		if(sector != null && oldState == STATE.WORKING) {
			if(!sector.unattachEmployee(this)) {
				MainController.logError("trying to release working place(sector "+sector.getSector().getId()
						+") while it is not occupied for employee "+getEmployee().getId());
			}
		}
		ownerFactoryAgent.getSocketServer().spreadEmployeeState(
				ownerFactoryAgent, 
				new NewState(employee.getId(), state, sectorId));
		try {
			if(this.state == STATE.READY) {
				getOwnerFactoryAgent().getWorkflowCalculator().getEmployeeAssigner().assign(this);
			}
		} catch (InterruptedException e) {
			MainController.logError("ERROR employee "+ getEmployee().getId() +": could not propely calculate workflow");
		}
	}

	public STATE getState() {
		return state;
	}

	@Override
	public synchronized void waitForMessage() throws InterruptedException {
		while(messageQueue.isEmpty()) {
			wait();
		}
	}

	@Override
	public synchronized void receiveMessage(Message message) throws InterruptedException {
		messageQueue.put(message);
		notifyAll();
	}
	
	

	@Override
	public String toString() {
		return "[employee " + employee.getId() + ", "
					+ employee.getFirstName() +" "+ employee.getLastName() + ", " + behaviour.getLabel() + "]";
	}

	@Override
	public synchronized void sendMessage(Message message, Agent receiver) throws InterruptedException {
		receiver.receiveMessage(message);
	}

	public Employee getEmployee() {
		return employee;
	}

	public List<EmployeeEntitlement> getEmployeeEntitlements() {
		return employeeEntitlements;
	}

	public FactoryAgent getOwnerFactoryAgent() {
		return ownerFactoryAgent;
	}
	
	
}
