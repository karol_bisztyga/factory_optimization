package com.bisztyga.factory_simulator.agents.communication;

import java.util.Map;

import com.bisztyga.factory_simulator.agents.Agent;

public interface Message {
	
	public enum CATEGORY {
		REQUEST,
		RESPONSE
	}
	
	public enum TYPE {
		EMPLOYEE_NEW_WORKPLACE,
		BREAK
	}
	
	public enum CONTENT_CODE {
		TAKING_TASK_DECISION,
		BREAK_DECISION,
		SECTOR_S
	}
	
	public CATEGORY getCategory();
	public Map<CONTENT_CODE, Object> getContent();
	public Agent getSender();
	public TYPE getType();
	
}
