package com.bisztyga.factory_simulator.agents.communication;

import java.util.Map;

import com.bisztyga.factory_simulator.agents.Agent;

public class Request implements Message {
	
	private final Map<CONTENT_CODE, Object> content;
	private final TYPE type;
	private final Agent sender;
	
	public Request(Map<CONTENT_CODE, Object> content, TYPE type, Agent sender) {
		super();
		this.content = content;
		this.type = type;
		this.sender = sender;
	}
	public Map<CONTENT_CODE, Object> getContent() {
		return content;
	}
	public TYPE getType() {
		return type;
	}
	public Agent getSender() {
		return sender;
	}
	@Override
	public CATEGORY getCategory() {
		return CATEGORY.REQUEST;
	}
	
	
}
