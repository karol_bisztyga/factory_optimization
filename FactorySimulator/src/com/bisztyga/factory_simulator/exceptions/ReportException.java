package com.bisztyga.factory_simulator.exceptions;

public class ReportException extends Exception {
	private static final long serialVersionUID = 1L;
	public ReportException(String s) {
		super(s);
	}
}
