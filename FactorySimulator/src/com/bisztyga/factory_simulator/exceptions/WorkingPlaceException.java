package com.bisztyga.factory_simulator.exceptions;

public class WorkingPlaceException extends Exception {
	private static final long serialVersionUID = 1L;
	public WorkingPlaceException(String s) {
		super(s);
	}
}
