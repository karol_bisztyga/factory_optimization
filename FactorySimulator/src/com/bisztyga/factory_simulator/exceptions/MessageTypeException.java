package com.bisztyga.factory_simulator.exceptions;

import com.bisztyga.factory_simulator.agents.communication.Message.TYPE;

public class MessageTypeException extends Exception {
	private static final long serialVersionUID = 1L;

	public MessageTypeException(String s) {
		super(s);
	}
	
	public MessageTypeException(TYPE messageType) {
		this("unexpected message type: " + messageType);
	}
}
