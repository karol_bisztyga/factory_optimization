package com.bisztyga.factory_simulator.exceptions;

public class EmployeeStateException extends Exception {
	private static final long serialVersionUID = 1L;

	public EmployeeStateException(String s) {
		super(s);
	}
}
