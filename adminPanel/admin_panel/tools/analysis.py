import os
from web.settings import BASE_DIR
from admin_panel.models import *
import datetime
from admin_panel.tools import dbTools

def getDate(timestamp_ms):
	return datetime.datetime.fromtimestamp(int(timestamp_ms)/1000).strftime('%Y-%m-%d')

def updateDatabase():
	filesPath = os.path.join(BASE_DIR+os.path.sep+".."+os.path.sep+"mutualFiles"+os.path.sep+"databaseScripts")
	if os.path.isdir(filesPath):
		files = [file for file in os.listdir(filesPath)]
		for file in files:
			with open(filesPath+os.path.sep+file) as f:
				print("processing file: ", file)
				records = f.readline()
				records = records.split(";")
				for record in records:
					values = record.split(",")
					if len(values) != 5:
						continue
					try:
						factory = Factory.objects.get(id=values[1])
						employee = Employee.objects.get(id=values[2])
						sector = Factorymapsector.objects.get(id=values[3])
						report = Report(date=values[0],factory=factory,employee=employee,sector=sector,period=values[4],maxperiod=factory.workinghours*60,workingplaces=sector.machine.workingplaces,totalworkingplaces=dbTools.getFactoryWorkingPlaces(factory.id))
						report.save()
					except (Factory.DoesNotExist,Factorymapsector.DoesNotExist,Employee.DoesNotExist) as e:
						print(e)
			os.remove(filesPath+os.path.sep+file)