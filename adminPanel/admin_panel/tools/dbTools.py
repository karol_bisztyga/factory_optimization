from admin_panel.models import *

def getFactoryWorkingPlaces(factoryId):
	workingPlaces = 0
	try:
		factory = Factory.objects.get(id=factoryId)
		fmap = Factorymap.objects.get(factory=factory)
		sectors = Factorymapsector.objects.filter(factorymap=fmap)
		for sector in sectors:
			workingPlaces += sector.machine.workingplaces
	except (Factory.DoesNotExist,) as e:
		print(e)
	return workingPlaces