import re

def validateUserData(firstname, lastname, email, phone):
	return (len(firstname) > 0 
		and len(lastname) > 0 
		and len(email) > 0 
		and len(phone) > 0 
		and re.compile("^[0-9]+$").match(phone)
		and re.compile("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$").match(email))