$(document).ready(function() {

	$(".remove-factory-decide").click(function() {
		var id = $(this).attr("id").split("-")[3];
		$("#remove-factory-form-"+id).css("display","block");
	});

	$(".remove-factory-cancel").click(function() {
		var id = $(this).attr("id").split("-")[3];
		$("#remove-factory-form-"+id).css("display","none");
	});

});