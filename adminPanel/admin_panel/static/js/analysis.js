$(document).ready(function() {

	//setting up token for ajax requests
	(function() {
		var csrftoken = $.cookie('csrftoken');
		function csrfSafeMethod(method) {
		    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}
		$.ajaxSetup({
		    beforeSend: function(xhr, settings) {
		        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
		            xhr.setRequestHeader("X-CSRFToken", csrftoken);
		        }
		    }
		});
	})();

	var currentData = null;
	var workingTime = 0;
	var ctx = document.getElementById("canvas-chart").getContext('2d');
	var chart = null;
	var currentDay = null;

	loadDayData();
	checkChartTypeVisibility();

	Chart.plugins.register({
	    beforeDraw: function(c) {
	        var ctx = c.chart.ctx;
	        ctx.fillStyle = 'white';
	        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
	    }
	});

	function loadDayData() {
		currentDay = $("#select-day").val();
		$.ajax({
			url: $("#url-get-data").val(),
			dataType: 'json',
			type: "POST",
			data: {
				day: currentDay,
			},
			success: function (data) {
				currentData = data.data;
				workingTime = data.workingtime;
				//console.log(currentData);
				displayData();
			},
			error: function (request, status, error) {
		        console.log(error);
		    }
		});
	}

	function displayData() {
		if(currentData == null) {
			return;
		}
		var chartType = $("#select-chart").val();
		var fixedData = [];
		var labels = [];
		var values = [];
		if(currentDay === "summary") {
			for(var i=0 ; i<currentData.length ; ++i) {
				for(k in currentData[i]) {
					var date = new Date(parseInt(k))
					labels[labels.length] = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
					values[values.length] = (currentData[i][k]).toFixed(2);
				}
			}
		} else {
			if(chartType == "employees") {
				var maxperiod = null;
				for(var i=0 ; i<currentData.length ; ++i) {
					var item = currentData[i];
					if(maxperiod == null) {
						maxperiod = item.maxperiod;
					}
					if(!fixedData.hasOwnProperty(item.employee)) {
						fixedData[item.employee] = 0;
					}
					fixedData[item.employee] += item.period;
				}
				for(var i in fixedData) {
					fixedData[i] = (fixedData[i]/maxperiod*100).toFixed(2);
				}
				for(var i in fixedData) {
					var item = fixedData[i];
					labels[labels.length] = i;
					values[values.length] = item;
				}
			} else if(chartType == "sectors") {
				for(var i=0 ; i<currentData.length ; ++i) {
					var item = currentData[i];
					if(!fixedData.hasOwnProperty("s"+item.sector)) {
						fixedData["s"+item.sector] = {
							period: 0,
							machine: item.machine,
							workingplaces: item.workingplaces,
							maxperiod: item.maxperiod
						};
					}
					fixedData["s"+item.sector]["period"] += item.period;
				}
				for(var i in fixedData) {
					var item = fixedData[i];
					labels[labels.length] = item.machine;
					values[values.length] = ((item.period/item.workingplaces)/item.maxperiod*100).toFixed(2);
				}
			}
		}
		if(chart !== null) {
			chart.destroy();
		}
		chart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: labels,
		        datasets: [{
		            label: 'percentage efficiency',
		            data: values,
		            borderWidth: 1
		        }]
		    },
		    options: {
                maintainAspectRatio: false,
            }
		});
		countResults(values);
	}

	//events

	$("#select-day").change(function() {
		loadDayData();
		checkChartTypeVisibility();
	});

	$("#select-chart").change(function() {
		displayData();
	});

	function checkChartTypeVisibility() {
		var day = $("#select-day").val();
		var display = (day == "summary") ? "none" : "inline-block" ;
		$("#select-chart-wrapper").css("display", display);
	}

	function countResults(values) {
		var data = [];
		for(var i=0 ; i<values.length ; ++i) {
			data[data.length] = values[i];
		}
		console.log(data);
		var min = Math.min.apply(Math, data);
		var max = Math.max.apply(Math, data);
		var avg = 0;
		for(var i=0 ; i<data.length ; ++i) {
			avg += parseFloat(data[i]);
		}
		avg /= data.length;
		var diff = max-min;
		$("#result-min").html(min);
		$("#result-max").html(max);
		$("#result-avg").html(avg);
		$("#result-diff").html(diff);
	}

})