$(document).ready(function() {

	$(".roll-adding-form").click(function() {
		var targetElement = $(this).attr("id").split("-")[3];
		var classes = $(this).attr("class").split(' ');
		for(c in classes) {
			if(classes[c] == "glyphicon-plus") {
				hide($(".roll-adding-form"), $(".adding-form"));
				show($(this), $("#adding-form-"+targetElement));
			} else if(classes[c] == "glyphicon-minus") {
				hide($(this), $("#adding-form-"+targetElement));
			}
		}
	});

	function show(button, element) {
		button.removeClass("glyphicon-plus");
		button.addClass("glyphicon-minus");
		element.css("display","block");
	}

	function hide(button, element) {
		button.removeClass("glyphicon-minus");
		button.addClass("glyphicon-plus");
		element.css("display","none");
	}

});