$(window).load(function() {
	
	var clockCanvas=document.getElementById("clock");
	var clockCtx=clockCanvas.getContext("2d");
	var clockSize = 100;
	clockCanvas.width = clockSize;
	clockCanvas.height = clockSize;
	clockCtx.translate(clockCanvas.width/2, clockCanvas.height/2);

	var mapCanvas = document.getElementById("map");
	var mapCtx = mapCanvas.getContext("2d");

	var part = 0;
	var max = 480;

	var port = 1234;
	var ws = new WebSocket("ws://127.0.0.1:"+ port);

	var map = {
		cellSize: 20,
		width: 0,
		height: 0,
		sectors: [],
		machines: [],
		employees: [],
		workingData: [],
		getEmployeeById: function(id) {
			for(var i=0 ; i<map.employees.length ; ++i) {
				var e = map.employees[i];
				if(e.id == id) return e;
			}
			return null;
		},
	};

	var stateColors = {
		"NOT_PRESENT": "#A0A0A0",
		"WORKING": "#00FF00",
		"BREAK": "#FF0000",
		"READY": "#0000FF",
	};

	ws.onopen = function(e) {
		console.log("open");

		var factoryId = $("#factory-id").html();
		if( factoryId.length === 0 ) {
			return;
		}
		ws.send("factory:"+factoryId);

		$("#connection-status").html("connected");
		$("#connection-status").css("color", "#0F0");

		$("#retry-button").css("display", "none");
	};

	ws.onmessage = function (e) { 
		var data = JSON.parse(e.data);
		var type = data["type"];
		var value = data["value"];
		switch(type) {
			case "CLOCK": {
				if(value == "0") {
					$("#clock-label").html("the day of work has ended");
					return;
				}
				var valueArr = value.split("/");
				var currentValue = valueArr[0];
				var maxValue = valueArr[1];
				$("#clock-label").html("minutes of work left: " + (maxValue-currentValue));
				drawClock(currentValue/maxValue)
				break;
			}
			case "INTRODUCING_DATA": {
				value = JSON.parse(value);

				map.width = value.factoryMap.width;
				map.height = value.factoryMap.height;
				var width = map.width*map.cellSize;
				var height = map.height*map.cellSize;
				$("#map").css("width", width+"px");
				$("#map").css("height", height+"px");
				mapCtx.canvas.width = width;
				mapCtx.canvas.height = height;

				map.sectors = value.sectors;
				map.machines = value.machines;
				map.employees = value.employees;
				map.workingData = value.workingData;

				for(var i=0 ; i<map.employees.length ; ++i) {
					map.employees[i]["state"] = value["employeesStates"][map.employees[i]["id"]];
				}

				console.log(map);

				drawMap();
				displayWorkingEmployees();
				break;
			}
			case "EMPLOYEE_NEW_STATE": {
				value = JSON.parse(value);
				console.log(value);
				for(var i=0 ; i<map.employees.length ; ++i) {
					var emp = map.employees[i];
					if(emp["id"] == value["employeeId"]) {
						emp["state"] = value["state"];
						map.workingData.employeesToSectors[value["employeeId"]] = value["sectorId"];
						if(emp["state"] != "WORKING") {
							map.workingData.employeesToSectors[emp.id] = 0;
						}
						displayWorkingEmployees();
						return;
					}
				}
				break;
			}
			default: {
				console.log("default:");
				console.log(data)
			}
		}
	};

	var workingEmployeesWrapper = $("#working-employees-wrapper");
	var freeEmployeesWrapper = $("#free-employees-wrapper");

	function displayWorkingEmployees() {
		console.log(map.workingData);
		workingEmployeesWrapper.empty();
		var table = $(document.createElement("table"));
		table.attr("border", "1");
		var headArray = ["sector", "machine", "working places"];
		var tr = document.createElement("tr");
		for(var i=0 ; i<headArray.length ; ++i) {
			var td = document.createElement("td");
			$(td).html(headArray[i]);
			tr.append(td);
		}
		table.append(tr);

		for(var i=0 ; i<map.sectors.length ; ++i) {
			var sector = map.sectors[i];
			var machine = getMachineOfSector(sector);
			if(machine === false) {
				console.log("no machine for sector: s" + sector.id);
				continue;
			}


			tr = document.createElement("tr");
			var td = document.createElement("td");
			$(td).html("s"+sector.id);
			tr.append(td);

			td = document.createElement("td");
			$(td).html(machine.name);
			tr.append(td);

			td = document.createElement("td");
			var ul = document.createElement("ul");
			var j=0;
			for(var empId in map.workingData.employeesToSectors) {
				var sect = map.workingData.employeesToSectors[empId];
				if(sect != sector.id) {
					continue;
				}
				var emp = map.getEmployeeById(empId);
				var li = document.createElement("li");
				$(li).attr("id", "sector-"+ sector.id +"-working-place-" + j);
				$(li).html(emp.firstName + " " + emp.lastName + "["+ emp.state +"]");
				$(li).css("color",stateColors[emp.state]);
				$(ul).append(li);
			}
			td.append(ul);
			tr.append(td);
			table.append(tr);
			++j;
		}

		workingEmployeesWrapper.append(table);

		freeEmployeesWrapper.empty();
		var ul = document.createElement("ul");
		for( var i=0 ; i<map.employees.length ; ++i ) {
			var emp = map.employees[i];
			if(map.workingData.employeesToSectors[emp.id] == 0) {
				var li = document.createElement("li");
				$(li).html(emp.firstName + " " + emp.lastName + "["+ emp.state +"]");
				$(li).css("color",stateColors[emp.state]);
				ul.append(li);
			}
			
		}
		freeEmployeesWrapper.append(ul);
	}

	function getMachineOfSector(sector) {
		for(var i=0 ; i<map.machines.length ; ++i) {
			var machine = map.machines[i];
			if( machine.id == sector.machine ) {
				return machine;
			}
		}
		return false;
	}

	ws.onclose = function() { 
		console.log("closed");
		$("#connection-status").html("disconnected");
		$("#connection-status").css("color", "#F00");

		$("#retry-button").css("display", "inline-block");
	};

	function drawMap() {
		mapCtx.clearRect(0, 0, mapCtx.canvas.width, mapCtx.canvas.height);
		mapCtx.strokeStyle = "#000000";

		for(var i=0 ; i<map.width ; ++i) {
			for(var j=0 ; j<map.height ; ++j) {
				drawCell(i, j);
			}
		}

		for(var i=0 ; i<map.sectors.length ; ++i) {
			drawSector(map.sectors[i], "s"+map.sectors[i].id);
		}
	}

	function drawSector(sector, id) {
		for(var i=sector.x ; i<sector.x+sector.width ; ++i) {
			for(var j=sector.y ; j<sector.y+sector.height ; ++j) {
				drawCell(i, j, "#FF0000");
				mapCtx.fillStyle = "#000000";
				mapCtx.font = "20px Arial";
				mapCtx.fillText(id,sector.x*map.cellSize, (sector.y*map.cellSize+(sector.y+sector.height)*map.cellSize)/2);
			}
		}
		mapCtx.beginPath();
		mapCtx.moveTo(sector.x*map.cellSize, sector.y*map.cellSize);
		mapCtx.lineTo((sector.x+sector.width)*map.cellSize, sector.y*map.cellSize);
		mapCtx.lineTo((sector.x+sector.width)*map.cellSize, (sector.y+sector.height)*map.cellSize);
		mapCtx.stroke();
	}

	function drawCell(x, y, fill) {
		if(typeof fill === 'undefined') {
			mapCtx.fillStyle = "#FFFFFF";
			mapCtx.fillRect(x*map.cellSize, y*map.cellSize, map.cellSize, map.cellSize);
			mapCtx.fillRect(x*map.cellSize+1, y*map.cellSize+1, map.cellSize-1, map.cellSize-1);
			mapCtx.beginPath();
			mapCtx.moveTo(x*map.cellSize, y*map.cellSize);
			mapCtx.lineTo((x+1)*map.cellSize, y*map.cellSize);
			mapCtx.lineTo((x+1)*map.cellSize, (y+1)*map.cellSize);
			mapCtx.stroke();
		} else {
			mapCtx.fillStyle = fill;
			mapCtx.fillRect(x*map.cellSize, y*map.cellSize, map.cellSize, map.cellSize);
		}
	}

	function drawClock(clockPart) {
		if(typeof clockPart === "undefined") clockPart = 1;
		clockCtx.clearRect(0, 0, clockCanvas.width, clockCanvas.height);
		clockCtx.fillStyle="#000";
		clockCtx.moveTo(0, 0);
		clockCtx.beginPath();
		clockCtx.arc(0, 0, clockSize/2.2, 0, Math.PI*2);
		clockCtx.lineTo(0, 0);
		clockCtx.fill();

		clockCtx.fillStyle="#FFF";
		clockCtx.beginPath();
		clockCtx.arc(0, 0, clockSize/2, Math.PI*-.5, Math.PI*-.5+(Math.PI*2*clockPart));
		clockCtx.lineTo(0, 0);
		clockCtx.fill();

		clockCtx.fillStyle="#FFF";
		clockCtx.beginPath();
		clockCtx.arc(0, 0, clockSize/2.5, 0, Math.PI*2);
		clockCtx.lineTo(0, 0);
		clockCtx.fill();
	}

	$("#retry-button").click(function() {
		location.reload();
	});
	
});