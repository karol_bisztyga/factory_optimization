$(document).ready(function() {

	//setting up token for ajax requests
	(function() {
		var csrftoken = $.cookie('csrftoken');
		function csrfSafeMethod(method) {
		    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}
		$.ajaxSetup({
		    beforeSend: function(xhr, settings) {
		        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
		            xhr.setRequestHeader("X-CSRFToken", csrftoken);
		        }
		    }
		});
	})();
	//

	var zoomMax = 150;
	var zoomMin = 50;

	var zoom = 100;
	var posX=0;
	var posY=0;
	var cellSize = 0;
	var map = [];
	var sectors = {};
	var hoveredSector = null;

	var canvas = document.getElementById("map");
	var ctx = canvas.getContext("2d");
	ctx.strokeStyle = "#000000";

	var mapMovement = {
		movingEnabled: true,
		moving: false,
		movingFlagX: -1,
		movingFlagY: -1,
		modX: 0,
		modY: 0,
	};

	var addingSector = {
		adding: false,
		width: 0,
		height: 0,
		selectionCurrentCell: null,
		priority: 0,
		checkPlace: function() {
			if(parseInt(this.selectionCurrentCell.x) < 0 || parseInt(this.selectionCurrentCell.y) < 0)
				return false;
			if(parseInt(this.selectionCurrentCell.x)+parseInt(this.width) > parseInt(map.length))
				return false;
			if(parseInt(this.selectionCurrentCell.y)+parseInt(this.height) > parseInt(map[0].length))
				return false;
			this.selectionCurrentCell.x = parseInt(this.selectionCurrentCell.x);
			this.selectionCurrentCell.y = parseInt(this.selectionCurrentCell.y);
			this.width = parseInt(this.width);
			this.height = parseInt(this.height);
			for(var i=this.selectionCurrentCell.x ; i<this.selectionCurrentCell.x+this.width ; ++i) {
				for(var j=this.selectionCurrentCell.y ; j<this.selectionCurrentCell.y+this.height ; ++j) {
					if(map[i][j]!==0) {
						return false;
					}
				}
			}
			return true;
		},
	};


	function createMap(obj) {
		map = [];
		sectors = {};
		for(var i=0; i<obj.width ; ++i) {
			map[i] = [];
			for(var j=0; j<obj.height ; ++j) {
				map[i][j] = 0;
			}
		}
		obj.sectors = JSON.parse(obj.sectors)
		for(var i=0 ; i<obj.sectors.length ; ++i) {
			var sector = obj.sectors[i].fields;
			sectors[obj.sectors[i].pk] = sector;
			sectors[obj.sectors[i].pk].id = obj.sectors[i].pk;
			for(var j=sector.x ; j<sector.x+sector.width ; ++j) {
				for(var k=sector.y ; k<sector.y+sector.height ; ++k) {
					map[j][k] = obj.sectors[i].pk;
				}
			}
		}
	}

	function getMap() {

		$.ajax({
			url: $("#url-get-map").val(),
			dataType: 'json',
			success: function (data) {
				createMap(data);
				resize();
				changeZoom(0);
				draw();
			},
			error: function (request, status, error) {
		        console.log(error);
		    }
		});

	};

	(function() {
		getMap();
	})();
	
	function draw() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		//draw basic board
		for(var i=0 ; i<map.length ; ++i) {
			for(var j=0 ; j<map[i].length ; ++j) {
				var element = map[i][j];
				ctx.fillStyle = "#FFFFFF";
				if(element > 0) {
					ctx.fillStyle = "#00FFFF";
				}
				if(hoveredSector && hoveredSector.id==element) {
					ctx.fillStyle = "#0000FF";
				}
				drawCell(i, j);
			}
		}
		//draw adding sector selection
		if(addingSector.adding && addingSector.selectionCurrentCell != null) {
			ctx.fillStyle = (addingSector.checkPlace()) ? "rgba(0,255,0,0.3)" : "rgba(255,0,0,0.3)";
			for(var i=0 ; i<addingSector.width ; ++i ) {
				for(var j=0 ; j<addingSector.height ; ++j ) {
					drawCell(addingSector.selectionCurrentCell.x+i, addingSector.selectionCurrentCell.y+j, false);
				}
			}
		}
	}

	function drawCell(x, y, frames) {
		ctx.fillRect((x+posX+mapMovement.modX)*cellSize, (y+posY+mapMovement.modY)*cellSize, cellSize, cellSize);
		if(typeof frames === "undefined" || frames) {
			ctx.beginPath();
			ctx.moveTo((x+posX+mapMovement.modX)*cellSize, (y+posY+mapMovement.modY)*cellSize,);
			ctx.lineTo((x+posX+mapMovement.modX+1)*cellSize, (y+posY+mapMovement.modY)*cellSize);
			ctx.lineTo((x+posX+mapMovement.modX+1)*cellSize, (y+posY+mapMovement.modY+1)*cellSize);
			ctx.stroke();
		}
	}

	$(".zoom").click(function(e) {
		var id = e.target.id;
		switch(id) {
			case "zoom-in":
				changeZoom(-100);
				break;
			case "zoom-out":
				changeZoom(100);
				break;
		}
	});

	$(".map-move").click(function(e) {
		var direction = e.target.id.split("-")[1];
		moveMap(direction);
	});
	
	$("#map-wrapper").mousedown(function(e) {
		if(addingSector.adding) {
			if(addingSector.checkPlace()) {

				$.ajax({
					url: $("#url-add-sector").val(),
					dataType: 'json',
					type: "POST",
					data: {
						width: addingSector.width,
						height: addingSector.height,
						x: addingSector.selectionCurrentCell.x,
						y: addingSector.selectionCurrentCell.y,
						priority: addingSector.priority,
						machine: $("#add-sector-machine").val()
					},
					success: function (data) {
						getMap();
					},
					error: function (request, status, error) {
				        console.log(error);
				    }
				});

				addingSector.adding = false;
				$("#sector-add-button").val("add");
			}
			draw();
		} else if(hoveredSector) {

			$.ajax({
				url: $("#url-get-sector").val().replace(0,hoveredSector.id),
				dataType: 'json',
				type: "POST",
				success: function (data) {
					loadSector(data);
				},
				error: function (request, status, error) {
			        console.log(error);
			    }
			});
		} else {
			if(mapMovement.movingEnabled) {
				var relCords = getCanvasCoordinates(e);
				mapMovement.moving = true;
				mapMovement.movingFlagX = relCords.x;
				mapMovement.movingFlagY = relCords.y;
			}
		}
			
	});

	function loadSector(sector) {
		$("#sector-details").empty();

		var positionDiv = $(document.createElement("div"));
		positionDiv.attr("class", "col-xs-12");
		positionDiv.html("position: ("+ sector.x +", "+ sector.y +")");

		var sizeDiv = $(document.createElement("div"));
		sizeDiv.attr("class", "col-xs-12");
		sizeDiv.html("size: ("+ sector.width +", "+ sector.height +")");

		var machineDiv = $(document.createElement("div"));
		machineDiv.attr("class", "col-xs-12");
		machineDiv.html("machine: "+ sector.machine);

		var priorityDiv = $(document.createElement("div"));
		priorityDiv.attr("class", "col-xs-12");
		priorityDiv.html("priority: "+ sector.priority);

		var workingPlacesDiv = $(document.createElement("div"));
		workingPlacesDiv.attr("class", "col-xs-12");
		workingPlacesDiv.html("working places: "+ sector.workingPlaces);

		var deleteButton = $(document.createElement("input"));
		deleteButton.attr("type", "button");
		deleteButton.val("delete");
		deleteButton.click(function() {
			$.ajax({
				url: $("#url-delete-sector").val().replace(0,sector.id),
				dataType: 'json',
				type: "POST",
				success: function (data) {
					if(data.response=='ok') {
						location.reload();
					}
				},
				error: function (request, status, error) {
			        console.log(error);
			    }
			});
		});

		$("#sector-details").append(positionDiv);
		$("#sector-details").append(sizeDiv);
		$("#sector-details").append(machineDiv);
		$("#sector-details").append(priorityDiv);
		$("#sector-details").append(workingPlacesDiv);
		$("#sector-details").append(deleteButton);
	}
	
	$("#map-wrapper").mousemove(function(e) {
		var relCords = getCanvasCoordinates(e);
		if(addingSector.adding) {
			addingSector.selectionCurrentCell = {
				x: Math.floor(relCords.x/cellSize)-posX,
				y: Math.floor(relCords.y/cellSize)-posY,
			};
			draw();
		} else {
			if(mapMovement.moving) {
				var diffX = relCords.x - mapMovement.movingFlagX;
				var diffY = relCords.y - mapMovement.movingFlagY;
				if(diffX != 0) {
					mapMovement.modX = parseInt(diffX / cellSize);
					mapMovement.modY = parseInt(diffY / cellSize);
				}
				draw();
			} else {
				var x = Math.floor(relCords.x/cellSize)-posX;
				var y = Math.floor(relCords.y/cellSize)-posY;
				if(typeof map[x]!=='undefined') {
					hoveredSector = sectors[map[x][y]];
					draw();
				}
			}
		}
		
	});

	$("#map-wrapper").mouseout(function(e) {
		addingSector.selectionCurrentCell = null;
		draw();
	});

	$("#map-wrapper").mouseup(function(e) {
		if(mapMovement.moving) {
			mapMovement.moving = false;
			posX += mapMovement.modX;
			posY += mapMovement.modY;
			mapMovement.modX = 0;
			mapMovement.modY = 0;
		}
	});

	$("#sector-add-button").click(function() {
		addingSector.adding = !addingSector.adding;
		if(addingSector.adding) {
			addingSector.width =  $("#add-sector-width").val();
			addingSector.height =  $("#add-sector-height").val();
			addingSector.priority = $("#add-sector-priority").val()
			$("#sector-add-button").val("cancel");
		} else {
			addingSector.selectionCurrentCell = null;
			$("#sector-add-button").val("add");
		}
	});

	function getCanvasCoordinates(event) {
		var parentOffset = $("#map").offset();
		var x = event.pageX - parentOffset.left;
   		var y = event.pageY - parentOffset.top;

   		var canvasX = x/$("#map").width()*canvas.width;
   		var canvasY = y/$("#map").height()*canvas.height;

   		return {x: canvasX, y: canvasY};
	}

	function moveMap(direction, distance) {
		if(typeof distance === 'undefined') distance = 1;
		switch(direction) {
			case "up": {
				posY += distance;
				break;
			}
			case "right": {
				posX -= distance;
				break;
			}
			case "down": {
				posY -= distance;
				break;
			}
			case "left": {
				posX += distance;
				break;
			}
		}
		draw();
	}

	$("#map-wrapper").bind("mousewheel", function(e) {
		changeZoom(e.originalEvent.deltaY);
	});

	$(window).resize(resize);

	function resize() {
		var canvasWidth = $(canvas).width();
		var canvasHeight = $(canvas).height();
		var padding = parseInt($("#map-right").css("padding"));

		$("#map-up").css("left",(canvasWidth/2+$("#map-up").width()/2)+"px");
		$("#map-right").css("top",(canvasHeight/2-$("#map-right").height()/2)+"px");
		$("#map-right").css("right",($("#map-right").width()/2)+"px");
		$("#map-down").css("left",(canvasWidth/2+$("#map-down").width()/2)+"px");
		$("#map-down").css("top",(canvasHeight-$("#map-down").height()-padding*2)+"px");
		$("#map-left").css("top",(canvasHeight/2-$("#map-left").height()/2)+"px");
	}

	function changeZoom(delta) {
		delta /= 10;
		zoom = Math.min(zoomMax, Math.max(zoomMin, zoom-delta));
		$("#current-zoom").html(zoom + "%");
		updateCellSize();
		draw();
	}

	function updateCellSize() {
		cellSize = zoom/5;
	}

});