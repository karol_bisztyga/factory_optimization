from django.shortcuts import render
from admin_panel.models import *
from admin_panel.tools import dataValidation, analysis, dbTools
from django.http import HttpResponseRedirect
from django.urls import reverse
import json
from django.http import HttpResponse
from django.core import serializers


##		VARIABLES	###

DEFAULT_MAP_WIDTH = 10
DEFAULT_MAP_HEIGHT = 10

###		GET 	###
from django.views.decorators.cache import never_cache

@never_cache
def index(request):
	return render(request, 'index.html', {
		"factories": Factory.objects.all(),
		"entitlements": Entitlement.objects.all()
	})

@never_cache
def factoryDetails(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		machines = []
		try:
			machines = Machine.objects.filter(factory=factoryId)
		except Machine.DoesNotExist:
			pass
		employees = []
		try:
			employees = Employee.objects.filter(factory=factoryId)
		except Employee.DoesNotExist:
			pass
		return render(request, 'factoryDetails.html', { 
			"factory":  factory,
			"machines": machines,
			"employees": employees
		})
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))


@never_cache
def machineDetails(request, machineId):
	try:
		machine = Machine.objects.get(id=machineId)
		factory = Factory.objects.get(id=machine.factory.id)
		machineEntitlementsIds = []
		machineEntitlements = set(Machineentitlementrequired.objects.filter(machine=machineId))
		for entitlement in machineEntitlements:
			machineEntitlementsIds.append(entitlement.entitlementrequired.id)
		unusedEntitlements = Entitlement.objects.exclude(id__in=machineEntitlementsIds)
		machineEntitlements = Entitlement.objects.filter(id__in=machineEntitlementsIds)
		return render(request, 'machineDetails.html', {
			"machine": machine,
			"factory": factory,
			"machineEntitlements": machineEntitlements,
			"unusedEntitlements": unusedEntitlements
		})
	except (Factory.DoesNotExist, Machine.DoesNotExist):
		return HttpResponseRedirect(reverse('admin_panel:index'))


@never_cache
def employeeDetails(request, employeeId):
	try:
		employee = Employee.objects.get(id=employeeId)
		factory = Factory.objects.get(id=employee.factory.id)
		empoyeeEntitlements = set(Employeeentitlement.objects.filter(employee=employeeId))
		empoyeeEntitlementsIds = []
		for entitlement in empoyeeEntitlements:
			empoyeeEntitlementsIds.append(entitlement.entitlement.id)
		unusedEntitlements = Entitlement.objects.exclude(id__in=empoyeeEntitlementsIds)
		empoyeeEntitlements = Entitlement.objects.filter(id__in=empoyeeEntitlementsIds)
		otherFactories = Factory.objects.exclude(id=factory.id)
		return render(request, 'employeeDetails.html', {
			"employee": employee,
			"factory": factory,
			"empoyeeEntitlements": empoyeeEntitlements,
			"unusedEntitlements": unusedEntitlements,
			"otherFactories": otherFactories
		})
	except (Factory.DoesNotExist, Employee.DoesNotExist):
		return HttpResponseRedirect(reverse('admin_panel:index'))

@never_cache
def viewMap(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		machines = []
		width = DEFAULT_MAP_WIDTH
		height = DEFAULT_MAP_HEIGHT
		try:
			factoryMap = Factorymap.objects.get(factory=factory)
			width = factoryMap.width
			height = factoryMap.height
		except Factorymap.DoesNotExist:
			Factorymap(factory = factory, width = DEFAULT_MAP_WIDTH, height = DEFAULT_MAP_HEIGHT).save()
		try:
			machines = Machine.objects.filter(factory=factoryId)
		except Machine.DoesNotExist:
			pass
		return render(request, 'map.html', { 
			"factory":  factory,
			"machines": machines,
			"width": width,
			"height": height,
		})
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))

@never_cache
def factoryStream(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		otherFactories = Factory.objects.exclude(id=factory.id)
		return render(request, 'factoryStream.html', { 
			"factory":  factory,
			"otherFactories": otherFactories
		})
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))

@never_cache
def factoryAnalysis(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		analysis.updateDatabase()
		reports = Report.objects.filter(factory=factory).order_by().values('date').distinct()
		reports = [{'readable': analysis.getDate(r['date']), 'value': r['date']} for r in reports]
		return render(request, 'factoryAnalysis.html', { 
			"factory":  factory,
			"reports": reports,
		})
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))

###		POST 	###

#FACTORY

def addFactory(request):
	address = request.POST.get("address", "")
	workingHours = request.POST.get("workinghours", "")
	restingHours = request.POST.get("restinghours", "")
	if len(address) > 0 and len(workingHours) > 0 and len(restingHours) > 0:
		workingHours = int(workingHours)
		restingHours = int(restingHours)
		if workingHours > restingHours:
			Factory(address=address, workinghours=workingHours, restinghours=restingHours).save()
	return HttpResponseRedirect(reverse('admin_panel:index'))

def removeFactory(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		machines = Machine.objects.filter(factory=factory)
		for machine in machines:
			Machineentitlementrequired.objects.filter(machine=machine).delete()
			machine.delete()
		maps = Factorymap.objects.filter(factory=factory)
		for mp in maps:
			sectors = Factorymapsector.objects.filter(factorymap=mp)
			for sector in sectors:
				workingPlaces = Workingplace.objects.filter(sector=sector)
				for workingPlace in workingPlaces:
					workingPlace.delete()
				sector.delete()
			mp.delete()
		employees = Employee.objects.filter(factory=factory)
		for employee in employees:
			Employeeentitlement.objects.filter(employee=employee).delete()
			employee.delete()
		factory.delete()
	except Factory.DoesNotExist:
		pass
	return HttpResponseRedirect(reverse('admin_panel:index'))

def updateFactory(request, factoryId):
	try:
		address = request.POST.get("address","")
		workingHours = request.POST.get("workinghours","")
		restingHours = request.POST.get("restinghours","")
		factory = Factory.objects.get(id=factoryId)
		if len(address) > 0 and len(workingHours) > 0 and len(restingHours) > 0:
			workingHours = int(workingHours)
			restingHours = int(restingHours)
			if workingHours > restingHours:
				factory.workinghours = workingHours
				factory.restinghours = restingHours
			factory.address = address
		factory.save()
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:factoryDetails', args=(factoryId,)))

#EMPLOYEE

def addEmployee(request):
	firstname = request.POST.get("firstname", "")
	lastname = request.POST.get("lastname", "")
	email = request.POST.get("email", "")
	phone = request.POST.get("phone", "")
	factoryId = int(request.POST.get("factory", 0))
	if dataValidation.validateUserData(firstname, lastname, email, phone):
		try:
			factory = Factory.objects.get(id=factoryId)
			Employee(firstname=firstname, lastname=lastname, email=email, phone=phone, factory=factory).save()
		except Factory.DoesNotExist:
			return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:factoryDetails', args=(factoryId,)))

def updateEmployee(request, employeeId):
	try:
		firstname = request.POST.get("firstname","")
		lastname = request.POST.get("lastname","")
		email = request.POST.get("email","")
		phone = request.POST.get("phone","")
		if dataValidation.validateUserData(firstname, lastname, email, phone):
			employee = Employee.objects.get(id=employeeId)
			employee.firstname = firstname
			employee.lastname = lastname
			employee.email = email
			employee.phone = phone
			employee.save()
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:employeeDetails', args=(employeeId,)))

def moveEmployeeToAnotherFactory(request, employeeId):
	try:
		factoryId = int(request.POST.get("factory",0))
		if factoryId != 0:
			factory = Factory.objects.get(id=factoryId)
			employee = Employee.objects.get(id=employeeId)
			employee.factory = factory
			employee.save()
	except (Employee.DoesNotExist, Factory.DoesNotExist):
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:employeeDetails', args=(employeeId,)))

def removeEmployee(request, factoryId, employeeId):
	try:
		employee = Employee.objects.get(id=employeeId)
		entitlements = Employeeentitlement.objects.filter(employee=employee)
		for ent in entitlements:
			ent.delete()
		employee.delete()
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:factoryDetails', args=(factoryId,)))

def addEntitlementToEmployee(request, employeeId):
	entitlementId = request.POST.get("entitlement", 0)
	try:
		entitlement = Entitlement.objects.get(id=entitlementId)
		employee = Employee.objects.get(id=employeeId)
		Employeeentitlement(employee=employee, entitlement=entitlement).save()
	except Entitlement.DoesNotExist:
		pass
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:employeeDetails', args=(employeeId,)))

def removeEntitlementFromEmployee(request, employeeId):
	entitlementId = request.POST.get("entitlement", 0)
	try:
		entitlement = Entitlement.objects.get(id=entitlementId)
		employee = Employee.objects.get(id=employeeId)
		Employeeentitlement.objects.get(employee=employee, entitlement=entitlement).delete()
	except (Entitlement.DoesNotExist, Employeeentitlement.DoesNotExist):
		pass
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:employeeDetails', args=(employeeId,)))

#MACHINE

def addMachine(request):
	name = request.POST.get("name", "")
	factoryId = int(request.POST.get("factory", 0))
	workingPlaces = int(request.POST.get("workingplaces", 1))
	factory = Factory.objects.get(id=factoryId)
	Machine(name=name, factory=factory, workingplaces=workingPlaces).save()
	return HttpResponseRedirect(reverse('admin_panel:factoryDetails', args=(factoryId,)))

def updateMachine(request, machineId):
	try:
		name = request.POST.get("name","")
		workingPlaces = int(request.POST.get("workingplaces",0))
		machine = Machine.objects.get(id=machineId)
		if len(name) > 0:
			machine.name = name
		if workingPlaces > 0:
			machine.workingplaces = workingPlaces
		machine.save()
	except Factory.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:machineDetails', args=(machineId,)))

def removeMachine(request, factoryId, machineId):
	try:
		machine = Machine.objects.get(id=machineId)
		entitlements = Machineentitlementrequired.objects.filter(machine=machine)
		for ent in entitlements:
			ent.delete()
		sectors = Factorymapsector.objects.filter(machine=machine)
		for sector in sectors:
			sector.delete()
		machine.delete()
	except Machine.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:factoryDetails', args=(factoryId,)))

def addEntitlementToMachine(request, machineId):
	entitlementId = request.POST.get("entitlement", 0)
	try:
		entitlement = Entitlement.objects.get(id=entitlementId)
		machine = Machine.objects.get(id=machineId)
		Machineentitlementrequired(machine=machine, entitlementrequired=entitlement).save()
	except Entitlement.DoesNotExist:
		pass
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:machineDetails', args=(machineId,)))

def removeEntitlementFromMachine(request, machineId):
	entitlementId = request.POST.get("entitlement", 0)
	try:
		entitlement = Entitlement.objects.get(id=entitlementId)
		machine = Machine.objects.get(id=machineId)
		Machineentitlementrequired.objects.get(machine=machine, entitlementrequired=entitlement).delete()
	except (Entitlement.DoesNotExist, Machineentitlementrequired.DoesNotExist):
		pass
	except Employee.DoesNotExist:
		return HttpResponseRedirect(reverse('admin_panel:index'))
	return HttpResponseRedirect(reverse('admin_panel:machineDetails', args=(machineId,)))



#ENTITLEMENT

def addEntitlement(request):
	name = request.POST.get("name", "")
	if len(name) > 0:
		Entitlement(name=name).save()
	return HttpResponseRedirect(reverse('admin_panel:index'))

def removeEntitlement(request, entitlementId):
	try:
		Entitlement.objects.get(id=entitlementId).delete()
		Employeeentitlement.objects.get(entitlement=entitlementId).delete()
		Machineentitlementrequired.objects.get(entitlementrequired=entitlementId).delete()
	except (Entitlement.DoesNotExist, Employeeentitlement.DoesNotExist, Machineentitlementrequired.DoesNotExist):
		pass
	return HttpResponseRedirect(reverse('admin_panel:index'))

def updateEntitlement(request, entitlementId):
	try:
		entitlement = Entitlement.objects.get(id=entitlementId)
		name = request.POST.get("name", "")
		if len(name) > 0:
			entitlement.name = name
			entitlement.save()
	except Entitlement.DoesNotExist:
		pass
	return HttpResponseRedirect(reverse('admin_panel:index'))


#MAP

def resetMap(request, factoryId):
	try:
		factory = Factory.objects.get(id=factoryId)
		factoryMap = Factorymap.objects.get(factory=factory)
		width = request.POST.get("width", "")
		height = request.POST.get("height", "")
		if len(width) > 0 and len(height) > 0:
			factoryMap.width = width
			factoryMap.height = height
			sectors = Factorymapsector.objects.filter(factorymap=factoryMap)
			for sector in sectors:
				sector.delete()
			factoryMap.save()
	except (Factory.DoesNotExist, Factorymap.DoesNotExist):
		pass
	return HttpResponseRedirect(reverse('admin_panel:viewMap', args=(factoryId,)))


###		AJAX 	###

def getMap(request, factoryId):
	width = DEFAULT_MAP_WIDTH
	height = DEFAULT_MAP_HEIGHT
	factory = Factory()
	sectors = {}
	try:
		factory = Factory.objects.get(id=factoryId)
		fmap = Factorymap.objects.get(factory=factory)
		sectors = Factorymapsector.objects.filter(factorymap=fmap)
		width = fmap.width
		height = fmap.height
	except Factory.DoesNotExist:
		pass
	except Factorymap.DoesNotExist:
		Factorymap(factory = factory, width = DEFAULT_MAP_WIDTH, height = DEFAULT_MAP_HEIGHT).save()
	return HttpResponse(
        json.dumps({
			'width': width,
			'height': height,
			'sectors': serializers.serialize('json', sectors)
    	}),
        content_type = 'application/javascript; charset=utf8'
    )

def getAnalysis(request, factoryId):
	data = []
	try:
		day = request.POST.get("day","")
		factory = Factory.objects.get(id=factoryId)
		reports = Report.objects.filter(factory=factory, date=day)
		if day == 'summary':
			summary = {}
			allreports = Report.objects.filter(factory=factory)
			totalWorkingPlaces = None
			maxPeriod = None
			for report in allreports:
				if totalWorkingPlaces == None:
					totalWorkingPlaces = report.totalworkingplaces
				if maxPeriod == None:
					maxPeriod = report.maxperiod
				if report.date not in summary:
					summary[report.date] = 0
				summary[report.date] += report.period
			for i in summary:
				averageTimeWorked = round(summary[i]/totalWorkingPlaces,2)
				percentageEfficiency = averageTimeWorked/maxPeriod*100
				data.append({i:percentageEfficiency})
		else:
			for report in reports:
				data.append({
					"employee": report.employee.firstname+' '+report.employee.lastname,
					'sector': report.sector.id,
					"machine": report.sector.machine.name,
					"period": report.period,
					'workingplaces': report.sector.machine.workingplaces,
					'maxperiod': report.maxperiod
				})
	except (Factory.DoesNotExist, Report.DoesNotExist):
		return HttpResponse(
	        json.dumps({
				'data': 'error',
	    	}),
	        content_type = 'application/javascript; charset=utf8'
	    )
	return HttpResponse(
        json.dumps({
			'data': data,
    	}),
        content_type = 'application/javascript; charset=utf8'
    )

def addSector(request, factoryId):
	width = request.POST.get("width", "")
	height = request.POST.get("height", "")
	x = request.POST.get("x", "")
	y = request.POST.get("y", "")
	machineId = request.POST.get("machine", "")
	priority = request.POST.get("priority", "")
	response = ""
	try:
		if len(width) > 0 and len(height) > 0 and len(x) > 0 and len(y) > 0 and len(machineId) > 0 and len(priority) > 0:
			factory = Factory.objects.get(id=factoryId)
			fmap = Factorymap.objects.get(factory=factory)
			sectors = Factorymapsector.objects.filter(factorymap=fmap)
			for sector in sectors:
				#print("checking sector ", sector.x, "," , sector.y )
				#checking collisions with sectors
				pass
			machine = Machine.objects.get(id=machineId)
			if int(priority) < 0:
				priority = 0
			sector = Factorymapsector(x=x,y=y,width=width,height=height,machine=machine, factorymap=fmap, priority=priority)
			sector.save()
			sector = Factorymapsector.objects.latest('id')
			for i in range(0,machine.workingplaces):
				Workingplace(sector=sector).save()
			response = "success"
		else:
			response = "not enough parameters"
	except Machine.DoesNotExist:
		response = "no such machine"
	except Factorymap.DoesNotExist:
		response = "no such map"
	except Factory.DoesNotExist:
		response = "no such factory"

	return HttpResponse(
        json.dumps({
			'response': response
    	}),
        content_type = 'application/javascript; charset=utf8'
    )


def getSector(request, factoryId, sectorId):
	response=""
	try:
		sector = Factorymapsector.objects.get(id=sectorId)
		if int(factoryId) != sector.factorymap.factory.id:
			response="this sector does not belong to the given factory ", factoryId, " ", sector.factorymap.factory.id
		else:
			return HttpResponse(
	        	json.dumps({
	        		'id': sector.id,
					'x': sector.x,
					'y': sector.y,
					'priority': sector.priority,
					'width': sector.width,
					'height': sector.height,
					'machine': sector.machine.name,
					'workingPlaces': sector.machine.workingplaces,
		    	}),
		        content_type = 'application/javascript; charset=utf8'
			)
	except (Factory.DoesNotExist, Factorymapsector.DoesNotExist):
		response = "no such factory/sector"

	return HttpResponse(
        json.dumps({
			'response': response
    	}),
        content_type = 'application/javascript; charset=utf8'
    )

def deleteSector(request, factoryId, sectorId):
	response = "ok"
	try:
		sector = Factorymapsector.objects.get(id=sectorId)
		Workingplace.objects.filter(sector=sector).delete()
		if sector.factorymap.factory.id != int(factoryId):
			response="this sector does not belong to the given factory ", factoryId, " ", sector.factorymap.factory.id
		else:
			sector.delete()
	except Factorymapsector.DoesNotExist:
		response = "sector does not exist"

	return HttpResponse(
        json.dumps({
			'response': response
    	}),
        content_type = 'application/javascript; charset=utf8'
    )