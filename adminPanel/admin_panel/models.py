from django.db import models


class Employee(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    firstname = models.CharField(db_column='firstName', max_length=255)  # Field name made lowercase.
    lastname = models.CharField(db_column='lastName', max_length=255)  # Field name made lowercase.
    email = models.CharField(max_length=255)
    phone = models.CharField(max_length=32)
    factory = models.ForeignKey('Factory', models.DO_NOTHING, db_column='factory')

    class Meta:
        managed = False
        db_table = 'Employee'


class Employeeentitlement(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    employee = models.ForeignKey(Employee, models.DO_NOTHING, db_column='employee')
    entitlement = models.ForeignKey('Entitlement', models.DO_NOTHING, db_column='entitlement')

    class Meta:
        managed = False
        db_table = 'EmployeeEntitlement'


class Entitlement(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'Entitlement'


class Factory(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    address = models.TextField()
    workinghours = models.IntegerField(db_column='workingHours')  # Field name made lowercase.
    restinghours = models.IntegerField(db_column='restingHours')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Factory'


class Factorymap(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    factory = models.ForeignKey(Factory, models.DO_NOTHING, db_column='factory', unique=True)
    width = models.IntegerField()
    height = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'FactoryMap'


class Factorymapsector(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    factorymap = models.ForeignKey(Factorymap, models.DO_NOTHING, db_column='factoryMap')  # Field name made lowercase.
    width = models.IntegerField()
    height = models.IntegerField()
    machine = models.ForeignKey('Machine', models.DO_NOTHING, db_column='machine')
    x = models.IntegerField()
    y = models.IntegerField()
    priority = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'FactoryMapSector'


class Machine(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    factory = models.ForeignKey(Factory, models.DO_NOTHING, db_column='factory')
    workingplaces = models.IntegerField(db_column='workingPlaces')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Machine'


class Machineentitlementrequired(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    machine = models.ForeignKey(Machine, models.DO_NOTHING, db_column='machine')
    entitlementrequired = models.ForeignKey(Entitlement, models.DO_NOTHING, db_column='entitlementRequired')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MachineEntitlementRequired'


class Report(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    date = models.TextField()  # This field type is a guess.
    factory = models.ForeignKey(Factory, models.DO_NOTHING, db_column='factory')
    employee = models.ForeignKey(Employee, models.DO_NOTHING, db_column='employee')
    sector = models.ForeignKey(Factorymapsector, models.DO_NOTHING, db_column='sector')
    period = models.IntegerField()
    maxperiod = models.IntegerField(db_column='maxPeriod')  # Field name made lowercase.
    workingplaces = models.IntegerField(db_column='workingPlaces')  # Field name made lowercase.
    totalworkingplaces = models.IntegerField(db_column='totalWorkingPlaces')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Report'


class Workingplace(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    sector = models.ForeignKey(Factorymapsector, models.DO_NOTHING, db_column='sector')
    occupiedby = models.ForeignKey(Employee, models.DO_NOTHING, db_column='occupiedBy', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'WorkingPlace'