from django.conf.urls import url

from . import views

app_name = 'admin_panel'

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^factory/(?P<factoryId>[0-9]+)/details/$', views.factoryDetails, name='factoryDetails'),
    url(r'^factory/add/$', views.addFactory, name='addFactory'),
    url(r'^factory/(?P<factoryId>[0-9]+)/remove/$', views.removeFactory, name='removeFactory'),
    url(r'^factory/(?P<factoryId>[0-9]+)/update/$', views.updateFactory, name='updateFactory'),
    url(r'^factory/(?P<factoryId>[0-9]+)/stream/$', views.factoryStream, name='factoryStream'),
    url(r'^factory/(?P<factoryId>[0-9]+)/analysis/$', views.factoryAnalysis, name='factoryAnalysis'),

    url(r'^employee/(?P<employeeId>[0-9]+)/details/$', views.employeeDetails, name='employeeDetails'),
    url(r'^employee/(?P<employeeId>[0-9]+)/update/$', views.updateEmployee, name='updateEmployee'),
    url(r'^factory/(?P<factoryId>[0-9]+)/employee/(?P<employeeId>[0-9]+)/remove/$', views.removeEmployee, name='removeEmployee'),
    url(r'^employee/add/$', views.addEmployee, name='addEmployee'),
    url(r'^employee/(?P<employeeId>[0-9]+)/entitlement/add/$', views.addEntitlementToEmployee, name='addEntitlementToEmployee'),
    url(r'^employee/(?P<employeeId>[0-9]+)/entitlement/remove/$', views.removeEntitlementFromEmployee, name='removeEntitlementFromEmployee'),
    url(r'^employee/(?P<employeeId>[0-9]+)/move/$', views.moveEmployeeToAnotherFactory, name='moveEmployeeToAnotherFactory'),

    url(r'^entitlement/add/$', views.addEntitlement, name='addEntitlement'),
    url(r'^entitlement/(?P<entitlementId>[0-9]+)/remove/$', views.removeEntitlement, name='removeEntitlement'),
    url(r'^entitlement/(?P<entitlementId>[0-9]+)/update/$', views.updateEntitlement, name='updateEntitlement'),

    url(r'^machine/add/$', views.addMachine, name='addMachine'),
    url(r'^machine/(?P<machineId>[0-9]+)/details/$', views.machineDetails, name='machineDetails'),
    url(r'^factory/(?P<factoryId>[0-9]+)/machine/(?P<machineId>[0-9]+)/remove/$', views.removeMachine, name='removeMachine'),
    url(r'^machine/(?P<machineId>[0-9]+)/update/$', views.updateMachine, name='updateMachine'),
    url(r'^machine/(?P<machineId>[0-9]+)/entitlement/add/$', views.addEntitlementToMachine, name='addEntitlementToMachine'),
    url(r'^machine/(?P<machineId>[0-9]+)/entitlement/remove/$', views.removeEntitlementFromMachine, name='removeEntitlementFromMachine'),

    url(r'^factory/(?P<factoryId>[0-9]+)/map/view/$', views.viewMap, name='viewMap'),
    url(r'^factory/(?P<factoryId>[0-9]+)/map/reset/$', views.resetMap, name='resetMap'),

#AJAX

    url(r'^ajax/factory/(?P<factoryId>[0-9]+)/map/get/$', views.getMap, name='getMap'),
    url(r'^ajax/factory/(?P<factoryId>[0-9]+)/map/sector/add/$', views.addSector, name='addSector'),
    url(r'^ajax/factory/(?P<factoryId>[0-9]+)/map/sector/get/(?P<sectorId>[0-9]+)/$', views.getSector, name='getSector'),
    url(r'^ajax/factory/(?P<factoryId>[0-9]+)/map/sector/delete/(?P<sectorId>[0-9]+)/$', views.deleteSector, name='deleteSector'),
    url(r'^ajax/factory/(?P<factoryId>[0-9]+)/analysis/get/$', views.getAnalysis, name='getAnalysis'),

]