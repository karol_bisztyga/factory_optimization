# Praca inżynierska AGH 2017. #

Temat: System do wspomagania pracy fabryki.

Autor: Karol Bisztyga

## 1. Zarys: ##

Aplikacja polega na najoptymalniejszym podziale pracy między pracowników fabryki, którą zarządza jednostka centralna. Zakładamy, że w fabryce są różne rodzaje taśm i urządzeń, z których część wymaga odpowiednich kwalifikacji do obsługi. Mają też różną ilość miejsc pracy. Jednostka zarządzająca pracownikami przy rozdzielaniu zadań będzie uwzględniała takie czynniki jak: przerwy dla pracowników, uprawnienia, priorytety maszyn(niekiedy od wyniku pracy jednej maszyny zależy praca innej, dlatego będą one miały priorytety produkcji) itp.

## 2. Elementy: ##

→ aplikacja wizualizująca bieżący stan fabryki, położenie pracowników itp.

→ moduł centrali:

*rozdzielanie zadań,

*dynamiczne gromadzenie danych i przygotowywanie raportów,

→ panel administracyjny:

*edycja fabryki oraz jej elementów(maszyn, pracowników i ich uprawnień, mapy fabryki itp),

→ aplikacja symulującą zachowania pracowników(aplikacja agentowa)

→ baza danych

## 3. Elementy techniczne do opisania w pracy: ##

→ wybranie odpowiedniego silnika bazy danych i porównanie silników pod różnymi względami

→ ogólnie opisany sposób działania aplikacji agentowej

→ znajdowanie najkrótszej drogi z punktu A do B(w algorytmie rozdzielającym pracę w fabryce)

## 4. Do napisania: ##

→ wizualizacja(web → js/python(django))

→ agenty pracowników, centrali i maszyn (java)

→ baza danych(SQLite)

→ panel administracyjny(web → js/python(django))

→ narzędzie obliczające czas pracy maszyn dla każdego miejsca pracy(java)

→ raporty(java, wizualizacje w web, eksport do innych formatów np .pdf)

## 5. Opis działania ##

→Moduł agentowy(java):

*rodzaje agentów:

-EmployeeAgent(agent pracownika)

-FactoryAgent(agent fabryki)

-SectorAgent(agent sektora; do każdego sektora jest przypisana maszyna)

*znaczenia poszczególnych wiadomości:

-są dwie kategorie wiadomości:

1. request = zapytanie

2. response = odpowiedź/powiadomienie

-istnieje kilka typów wiadomości. Poszczególne z nich mają rózne znaczenie w zależności jakiej są kategorii oraz gdzie i skąd są wysyłane:

| Type | Category | Sender | Receiver | Content | Meaning |
:---:|:---:|:---:|:---:|:---:|:---
| EMPLOYEE_NEW_WORKPLACE | REQUEST | FactoryAgent | EmployeeAgent | SECTOR_S | EmployeeAgent otrzymuje listę sektorów, w których może pracować |
| EMPLOYEE_NEW_WORKPLACE | RESPONSE | EmployeeAgent | FactoryAgent | SECTOR_S | Przesłanie decyzji(tak/nie) odnośnie stawienia się przez pracownika oraz rozpoczęcia pracy w nowym miejscu. Wysyła wybray SectorAgent lub null jeśli nie zdecydował się na żaden z proponowanych |
| BREAK | REQUEST | EmployeeAgent | FactoryAgent | null | Zapytanie o pójście na przerwę |
| BREAK | RESPONSE | FactoryAgent | EmployeeAgent | BREAK_DECISION | Odpowiedź na zapytanie o pójście na przerwę |

*sposób działania:

1. na początku jest uruchamiany agent fabryki, który odczytuje z bazy danych listę pracowników i sektorów i odpala odpowiednią ilość EmployeeAgent i SectorAgent. Oprócz tego startuje też InitialSectorTaskHandler

2. Algorytm zapełniania miejsc pracy w sektorze uruchamia się za każdym razem, gdy któryś z pracowników zmieni swój stan. W zależności od nowego stanu podejmowane są odpowiednie akcje:
-READY: algorytm bierze wszystkie sektory, w których są wolne miejsca pracy, odrzuca te, gdzie pracownik nie może pracować ze względu na brak uprawnień po czym sortuje je tak, aby najpierw były brane pod uwagę te z większym priorytetem(dla przypadków, gdzie priorytety są takie same najpierw idą te, które wymagają więcej uprawnień). Następnie wysyła pracownikowi propozycje pracy na kolejnych sektorach z otrzymanej listy. Jeśli odrzuci wszystkie propozycje jest dodawany na koniec kolejki pracowników oczekujących na zadania.
